Revanche
========

Revanche /rəˈvæntʃ/ (n.) - the policy of a state intent on regaining areas of its original territory that have been lost to other states as a result of war, a treaty signed under duress, etc. *

Revanche is a GUI application designed to act as a user interaction layer for managing a set of objects with complex schemata. You could compare it to a database frontend like phpMyAdmin or SQL Server Management Studio. Instead of SQL, Revanche uses YAML for everything - its type system, its schemata, and its serialized objects. Revanche is very flexible and can be configured to handle a wide variety of objects and heirarchies, but it is not meant to be a replacement for a real database. Its primary goal is to make navigating object heirarchies and editing object properties as effortless and natural as possible.

Revanche was originally built with the original intent of regaining control over the process of character creation and development in fiction writing, hence its name. Details about a fictional character are many and varied, but in my own experience, creating a new character tends to fall into an increasingly routine "template". Characters have names. They have hair color. They have a temperament. They have a style of dress. They are associated with a particular writing project. This program is an attempt to resolve the problem of writing endless word processor documents whose first paragraphs begin with things like "Name:", "Hair Color:", "Temperament:", et cetera, when in many cases the there are only a few possible things which can follow. This problem, I don't doubt, is not unique to fiction writing.

What Revanche Is
----------------

Revanche is a C# application written using Mono and Gtk#. It loads a framework you define in YAML and displays this framework to you in a nice GUI, allowing you to create and edit objects within said framework. The program is extremely flexible, and basically any tree of objects can be displayed, with whichever editable fields you like, using whatever values you feel necessary.

Revanche should run on Linux with Mono installed. To run on Windows, you'll need to install Gtk#.

Revanche makes use of [YamlDotNet](http://aaubry.net/pages/yamldotnet.html), available under the MIT license. Revanche itself is licensed under the GPL, version 3.0.

What Revanche Is Not
--------------------

Revanche is not an out-of-the-box solution. To make use of the program, you will need to define your own data structure, with the objects and details you want to see. A few example structures are available and included with the program (if you choose to set them up), and more documentation is available the STRUCTURE.md file.

\* "revanche." Dictionary.com Unabridged. Random House, Inc. 26 Mar. 2015. <Dictionary.com http://dictionary.reference.com/browse/revanche>.
