using System;
using System.Linq;
using Revanche.Models;
using Revanche.Lib;
using System.Text.RegularExpressions;

namespace Revanche
{
	public static class Locator
	{
		private static Regex titleFacet=new Regex(@"/?[^/]+(?:/[^/]+)*");

		private static Heirarchical[] descend(Heirarchical parent)
		{
			return parent.GetChildren().Union(
				from Heirarchical[] f in parent.GetCollections()
				from Heirarchical s in f
				select s
			).ToArray();
		}

		private static Identifiable recurseIdentifiableTitle(Heirarchical[] list,string[] specifier)
		{
			// This is a "best attempt" method - there is nothing saying two children of a given object cannot have the same title. If they do, you will get one of the two; that's your problem.
			foreach(Identifiable m in list) {
				if(m.Title==specifier[0]) {
					if(specifier.Length==1) {
						return m;
					} else {
						return Locator.recurseIdentifiableTitle(
							Locator.descend(m),
							specifier.Skip(1).ToArray()
						);
					}
				}
			}
			return null;
		}

		private static string[][] parse(string lookupCommand,Heirarchical parent=null){
			string[] commands=lookupCommand.Split(':');

			// protocol handlers
			if(commands[0]==Config.ID_PROTOCOL) {
				commands[0]="ID";
				if(commands[1].StartsWith("//")) {
					commands[1]=commands[1].Substring(2);
				}
			} else if(commands[0]==Config.TITLE_PROTOCOL) {
				commands[0]="TITLE";
				if(commands[1].StartsWith("//")) {
					commands[1]=commands[1].Substring(2);
				}
			}
				
			if(commands.Length==2) {
				commands=new string[]{ commands[0], "", commands[1] };
			}
			if(commands.Length!=3) {
				throw new ArgumentException("Invalid lookup command: "+lookupCommand);
			}
			string[] specifiers=commands[2].Split('/');
			if((commands[0]=="MODEL"||commands[0]=="ID")&&specifiers.Length!=2) {
				throw new ArgumentException("Invalid lookup command: "+lookupCommand);
			}
			if(specifiers[0]!=""&&parent==null) {
				throw new ArgumentException("Relative specifier, but no parent was provided (did you mean "+commands[0]+":"+commands[1]+":/"+commands[2]+"?)");
			}
			return new string[][]{ commands, specifiers };
		}

		public static Heirarchical Find(string lookupCommand,Heirarchical parent=null)
		{
			string[][] elems=Locator.parse(lookupCommand,parent);
			string[] commands=elems[0];
			string[] specifiers=elems[1];

			if(commands[1]!=""&&Profile.Current!=null&&commands[1]!=Profile.Current) {
				throw new ArgumentException("Inter-profile specifiers are not permitted. You tried to access the profile "+commands[1]+", but you are in profile "+Profile.Current);
			}

			Heirarchical[] toplevel=specifiers[0]==""?null:Locator.descend(parent);
			if(specifiers[0]=="") {
				specifiers=specifiers.Skip(1).ToArray();
			}
			
			switch(commands[0]) {
				case "MODEL":
					return RevModel.GetRegisteredModel(specifiers[0]);
				case "ID":
					return Identifiable.GetRegisteredIdentifiable(specifiers[0]);
				case "TITLE":
				case "":
					if(toplevel==null) {
						toplevel=Identifiable.NoParents.ToArray();
					}
					return Locator.recurseIdentifiableTitle(toplevel,specifiers);
			}
			
			throw new ArgumentException("Unknown command: "+commands[0]);
		}

		public static string GetProfile(string lookupCommand,Heirarchical parent=null){
			return Locator.parse(lookupCommand,parent)[0][1];
		}

		/// <summary>
		/// Fast comparison of beginning of string
		/// </summary>
		/// <param name="given">Given.</param>
		/// <param name="test">Test.</param>
		private static bool indexBeginEqual(string given,string test){
			if(test.Length>given.Length){
				return false;
			}
			for(int i=0;i<test.Length;i++){
				if(given[i]!=test[i]){
					return false;
				}
			}
			return true;
		}

		public static bool IsSpecifier(string test){
			string[][] components;
			try{
				components=Locator.parse(test);
			}catch(Exception){
				return false;
			}

			string[] commands=components[0];

			if(commands[0]=="MODEL") {
				return RevModel.IsValidId(commands[2].Substring(1));
			}
			else if(commands[0]=="TITLE"||commands[0]=="") {
				return Locator.titleFacet.IsMatch(commands[2]);
			}
			else if(commands[0]=="ID"){
				return Identifiable.IsValidId(commands[2].Substring(1));
			}

			return false;
		}
	}
}

