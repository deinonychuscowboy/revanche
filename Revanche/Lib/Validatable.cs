﻿using System;

namespace Revanche.Lib
{
	public interface Validatable
	{
		ValidationReport Validate();
	}
}

