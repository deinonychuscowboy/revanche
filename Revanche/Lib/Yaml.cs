using System;
using System.IO;
using System.Collections.Generic;
using YamlDotNet.Serialization;
using System.Runtime.Serialization;

namespace Revanche.Lib
{
	/// <summary>
	/// Yaml persistence layer
	/// </summary>
	public class Yaml<T>:Loader<T>{
		private Serializer serializer;
		private Deserializer deserializer;

		public Yaml(Serializer serializer,Deserializer deserializer)
		{
			this.serializer=serializer;
			this.deserializer=deserializer;
		}

		public List<T> Load(string path)
		{
			try {
                return this.deserializer.Deserialize<List<T>>(new StreamReader(path));
			} catch(YamlDotNet.Core.YamlException e) {
				ErrorPipe.Stack(e);
				throw new YamlDotNet.Core.YamlException(e.Start,e.End,e.Message+"\n"+path,e);
			}catch(Exception e){
				ErrorPipe.Stack(e);
				throw e;
			}
		}

		public void Save(List<T> types,string path)
		{
			if(File.Exists(path)){
				File.Delete(path);
			}
			using(StreamWriter streamWriter=new StreamWriter(path,false)) {
				this.serializer.Serialize(streamWriter,types);
				streamWriter.Flush();
			}
		}
	}
}

