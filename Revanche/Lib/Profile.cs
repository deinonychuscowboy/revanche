﻿using System;
using System.Linq;
using System.IO;

namespace Revanche.Lib
{
	public static class Profile
	{
		public static string Current{ get; set;}

		public static string[] Profiles
		{
			get{
				return (from string dir in Directory.EnumerateDirectories(Config.ROOT+"profiles") select Path.GetFileName(dir)).ToArray();
			}
		}

		public static bool HasDefault{
			get{
				return Config.DEFAULT_PROFILE!=""&&Profile.Profiles.Contains(Config.DEFAULT_PROFILE);
			}
		}

		public static string Default
		{
			get {
				return Config.DEFAULT_PROFILE!=""?Config.DEFAULT_PROFILE:"default";
			}
		}

		public static void Setup(){
			if(!Directory.Exists(Config.ROOT+"profiles")){
				// always give the user a default profile on first run
				Directory.CreateDirectory(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+"default");
				Config.DEFAULT_PROFILE="default";
				Config.Save();
				// migration from old directory structure
				if(Directory.Exists(Config.ROOT+"struct")){
					Directory.Move(Config.ROOT+"struct",Config.ROOT+"profiles"+Path.DirectorySeparatorChar+"default"+Path.DirectorySeparatorChar+"struct");
				} else{
					Directory.CreateDirectory(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+"default"+Path.DirectorySeparatorChar+"struct");
				}
				if(Directory.Exists(Config.ROOT+"data")){
					Directory.Move(Config.ROOT+"data",Config.ROOT+"profiles"+Path.DirectorySeparatorChar+"default"+Path.DirectorySeparatorChar+"data");
				} else{
					Directory.CreateDirectory(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+"default"+Path.DirectorySeparatorChar+"data");
				}
				if(Directory.Exists(Config.ROOT+"backup")){
					Directory.Move(Config.ROOT+"backup",Config.ROOT+"profiles"+Path.DirectorySeparatorChar+"default"+Path.DirectorySeparatorChar+"backup");
				} else{
					Directory.CreateDirectory(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+"default"+Path.DirectorySeparatorChar+"backup");
				}
			}
			if(!Directory.Exists(Config.ROOT+"trash")){
				Directory.CreateDirectory(Config.ROOT+"trash");
			}
		}

		public static void Load(string name){
			Current=name==""?null:name;
		}

		public static void Unload(){
			Current=null;
		}

		public static void Create(string name){
			Directory.CreateDirectory(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+name+Path.DirectorySeparatorChar+"struct");
			Directory.CreateDirectory(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+name+Path.DirectorySeparatorChar+"data");
			Directory.CreateDirectory(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+name+Path.DirectorySeparatorChar+"backup");
		}

		public static void Move(string name, string newname){
			Directory.Move(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+name,Config.ROOT+"profiles"+Path.DirectorySeparatorChar+newname);
			if(Config.DEFAULT_PROFILE==name) {
				Config.DEFAULT_PROFILE=newname;
				Config.Save();
			}
		}

		public static void Delete(string name){
			if(Directory.Exists(Config.ROOT+"trash"+Path.DirectorySeparatorChar+name)) {
				Directory.Delete(Config.ROOT+"trash"+Path.DirectorySeparatorChar+name,true);
			}
			Directory.Move(Config.ROOT+"profiles"+Path.DirectorySeparatorChar+name,Config.ROOT+"trash"+Path.DirectorySeparatorChar+name);
			if(Config.DEFAULT_PROFILE==name) {
				Config.DEFAULT_PROFILE="";
				Config.Save();
			}
		}

		public static string StructFolder
		{
			get{
				if(Current==null){
					throw new InvalidOperationException("No profile loaded!");
				}
				return Config.ROOT+"profiles"+Path.DirectorySeparatorChar+Current+Path.DirectorySeparatorChar+"struct";
			}
		}

		public static string DataFolder
		{
			get{
				if(Current==null){
					throw new InvalidOperationException("No profile loaded!");
				}
				return Config.ROOT+"profiles"+Path.DirectorySeparatorChar+Current+Path.DirectorySeparatorChar+"data";
			}
		}

		public static string BackupFolder
		{
			get{
				if(Current==null){
					throw new InvalidOperationException("No profile loaded!");
				}
				return Config.ROOT+"profiles"+Path.DirectorySeparatorChar+Current+Path.DirectorySeparatorChar+"backup";
			}
		}
	}
}

