using System;
using YamlDotNet.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;  
using YamlDotNet.Serialization.ObjectFactories;
using System.Linq;
using System.Reflection;

namespace Revanche.Lib
{
	/// <summary>
	/// Data management class which uses the YAML persistence layer to set up Revanche's type system
	/// </summary>
	public class YamlDataManager
	{
		private static Regex backupfolder=new Regex(@"^\d\d\d\d_\d\d_\d\d_\d\d_\d\d_\d\d$");
		private static Regex typesFiles=new Regex(@"^types_.*\.ya?ml$");
		private static Regex modelsFiles=new Regex(@"^models_.*\.ya?ml$");
		private static Regex dataFiles=new Regex(@"^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}\.ya?ml$");
		/// <summary>
		/// Constructs a YamlDataManager and loads the Revanche structure in the process (stage 1)
		/// </summary>
		public YamlDataManager()
		{
			if(!File.Exists(Profile.StructFolder+Path.DirectorySeparatorChar+"README")){
				Example.WriteReadmeFile();
			}

			Types.RevType.SetupBasicTypes();

			Lib.Yaml<Types.RevType> tl=new Lib.Yaml<Types.RevType>(new Serializer(),new Deserializer());
			Lib.Yaml<Models.RevModel> ml=new Lib.Yaml<Models.RevModel>(new Serializer(),new Deserializer());

			string lastFilename="";
			try{
				foreach(string filename in Directory.EnumerateFiles(Profile.StructFolder).Where(x=>typesFiles.IsMatch(Path.GetFileName(x)))){
					lastFilename=filename;
					List<Types.RevType> toCheck=tl.Load(filename);
					if(toCheck!=null){
						// okay for the user to specify no types if they just intend to use the defaults
						foreach(Types.RevType type in toCheck){
							// this combination will only occur if the user has not explicitly set a default.
							// If they do set a default, and the default is null or blank, the content type will be changed to NULLABLE_ENUMERATED
							if(type.ContentType==Revanche.Types.Content.ENUMERATED&&(type.DefaultValue==null||type.DefaultValue.ToString()=="")){
								if(type.Id=="COLOR"){
									type.ContentType=Revanche.Types.Content.NULLABLE_ENUMERATED;
								} else{
									throw new Exception("The "+type.Id+" type requires an explicit DefaultValue.");
								}
							}
						}
					}
				}
				foreach(string filename in Directory.EnumerateFiles(Profile.StructFolder).Where(x=>modelsFiles.IsMatch(Path.GetFileName(x)))){
					lastFilename=filename;
					ml.Load(filename);
				}
			} catch(YamlDotNet.Core.YamlException e){
				string specialMessage;
				if(e.InnerException!=null&&(new Regex(@"Property '")).IsMatch(e.InnerException.Message)){
					specialMessage="'"+e.InnerException.Message.Split('\'')[1]+"' is not a known element.";
				} else{
					specialMessage=(new Regex(@"\([^\)]*\)")).Replace((e.InnerException??e).Message,"");
				}
				throw new FormatException("Malformed YAML in file "+lastFilename+"\nLine "+e.End.Line+": "+specialMessage);
			}
		}

		/// <summary>
		/// Loads the Revanche data (stage 2).
		/// </summary>
		public void Load()
		{
			Lib.Yaml<Models.Identifiable> il=new Lib.Yaml<Revanche.Models.Identifiable>(new Serializer(),new Deserializer());
			foreach(string filename in Directory.EnumerateFiles(Profile.DataFolder).Where(x=>dataFiles.IsMatch(Path.GetFileName(x)))){
				il.Load(filename);
			}
		}

		private int[] dateMinus(int[] first, int[] second){
			if(first.Length!=second.Length){
				throw new ArgumentException();
			}
			int[] output=new int[first.Length];
			for(int i=0;i<first.Length;i++) {
				output[i]=first[i]-second[i];
			}
			return output;
		}

		private string keyTranslate(int[] dateDiff){
			string output="identical";
			string[] values=new string[]{
				"years",
				"months",
				"days",
				"hours",
				"minutes",
				"seconds"
			};
			for(int i=dateDiff.Length-1;i>=0;i--){
				if(dateDiff[i]>0){
					output=values[i];
				}
			}
			return output;
		}

		/// <summary>
		/// Cycles and manages backups.
		/// </summary>
		public void Backup()
		{
			if(Controller.Dirty) {
				// create a backup folder from the current data folder
				if(Directory.Exists(Profile.DataFolder)) {
					Directory.Move(Profile.DataFolder,Profile.BackupFolder+Path.DirectorySeparatorChar+DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"));
				}
				Directory.CreateDirectory(Profile.DataFolder);
				int[] now=DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss").Split('_').Select(x => int.Parse(x)).ToArray();
				// obtain a grouping of all existing backup folders based on how long ago they were created
				IEnumerable<IGrouping<string,string>> backups=Directory.EnumerateDirectories(Profile.BackupFolder)
					.Where(x=>YamlDataManager.backupfolder.IsMatch(Path.GetFileName(x)))
					.GroupBy(x=>this.keyTranslate(this.dateMinus(now,Path.GetFileName(x).Split('_').Select(y=>int.Parse(y)).ToArray())));

				// limit each group to 10 elements
				foreach(IGrouping<string,string> grouping in backups) {
					int extras=grouping.Count()-10;
					IEnumerable<string> dirs=grouping.ToArray();
					if(extras>0) {
						// if more than 10 backups exist in this timespan, remove backups spaced evenly through the grouping, and avoid removing the oldest and newest ones
						int chunkSize=dirs.Count()/(extras+1);
						// this may not remove as many as needed if there are many more backups than the threshhold, but that's okay
						while(extras>0&&dirs.Count()>chunkSize) {
							dirs=dirs.Skip(chunkSize).ToArray();
							Directory.Delete(dirs.First(),true);
							dirs.Skip(1);
							extras--;
						}
					}
				}
			}
		}

		/// <summary>
		/// Saves the in-ram data structures through the persistence layer.
		/// </summary>
		public void Save()
		{
			if(Controller.Dirty) {
				this.innerSave(Profile.DataFolder);
			}
		}

		/// <summary>
		/// Emergency save to well-known location in the event of unplanned shutdown
		/// </summary>
		public void EmgSave()
		{
			this.innerSave(Config.ROOT+"recovery"+Path.DirectorySeparatorChar+DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"));
		}

		private void innerSave(string path){
			Lib.Yaml<Models.Identifiable> il=new Lib.Yaml<Revanche.Models.Identifiable>(new Serializer(),new Deserializer());
			foreach(Models.Identifiable i in Models.Identifiable.NoParents){
				List<Models.Identifiable> flatList=i.Flatten();
				// reverse so that children are serialized first to make a nice clean flat list instead of a descending heirarchy of indents
				flatList.Reverse();
				il.Save(flatList,path+Path.DirectorySeparatorChar+i.Id+".yml");
			}
		}
	}
}

