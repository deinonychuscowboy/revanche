﻿using System;
using System.Collections.Generic;

namespace Revanche.Lib
{
	[Flags]
	public enum ValidationFinding{
		UNVALIDATED=0,
		INVALID=1,
		UNRESOLVED=2,
		NEW_FIELD=4,
		DELETED_FIELD=8,
		DEFAULT_FIELD=16,
		REFERENCE=32,
	}
	public class ValidationReport
	{
		private ValidationFinding findings;
		private Dictionary<string,Dictionary<string,Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>>> data;

		public ValidationFinding Findings {
			get {
				return this.findings;
			}
		}

		private string getLongTitle(RegisteredObject obj){
			if(obj.GetType()==typeof(Models.Identifiable)){
				string titleString=(obj as Models.Identifiable).Title;
				Models.Identifiable iter=(obj as Models.Identifiable).Parent;
				while(iter!=null){
					titleString=iter.Title+"/"+titleString;
					iter=iter.Parent;
				}
				return titleString;
			} else{
				return obj.ToString();
			}
		}

		public List<string> Data {
			get {
				List<string> pretty=new List<string>();
				foreach(KeyValuePair<string,Dictionary<string,Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>>> upstream in this.data){
					foreach(KeyValuePair<string,Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>> subjectType in upstream.Value){
						foreach(KeyValuePair<KeyValuePair<string,ValidationFinding>,List<string>> subject in subjectType.Value){
							foreach(string downstream in subject.Value){
								string val=((subject.Key.Value&(ValidationFinding.INVALID|ValidationFinding.UNRESOLVED))>0?"!!! ":(subject.Key.Value&ValidationFinding.INVALID)>0?"--- ":"    ");
								val+=(subjectType.Key+" "+subject.Key.Key);
								val+=((subject.Key.Value&ValidationFinding.NEW_FIELD)>0||(subject.Key.Value&ValidationFinding.REFERENCE)>0?" is in ":(subject.Key.Value&(ValidationFinding.DELETED_FIELD|ValidationFinding.DEFAULT_FIELD))>0?" is not in ":": ");
								val+=(this.getLongTitle(RegisteredObject.GetRegisteredObject(upstream.Key)));
								if(upstream.Key!=downstream) {
									val+=((subject.Key.Value&ValidationFinding.NEW_FIELD)>0?" but is not in ":(subject.Key.Value&ValidationFinding.DELETED_FIELD)>0?" but has data in ":(subject.Key.Value&ValidationFinding.DEFAULT_FIELD)>0?" but is the default for ":" ==> ");
									val+=(this.getLongTitle(RegisteredObject.GetRegisteredObject(downstream)));
								}
								pretty.Add(val);
							}
						}
					}
				}
				return pretty;
			}
		}

		public ValidationReport()
		{
			this.findings=ValidationFinding.UNVALIDATED;
			this.data=new Dictionary<string,Dictionary<string,Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>>>();
		}
		public void AddFinding(ValidationFinding finding,string subjectType,string subject,string upstream,string downstream){
			KeyValuePair<string,ValidationFinding> changeEvent=new KeyValuePair<string, ValidationFinding>(subject,finding);
			if(!this.data.ContainsKey(upstream)){
				this.data[upstream]=new Dictionary<string,Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>>();
			}
			if(!this.data[upstream].ContainsKey(subjectType)){
				this.data[upstream][subjectType]=new Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>();
			}
			if(!this.data[upstream][subjectType].ContainsKey(changeEvent)){
				this.data[upstream][subjectType][changeEvent]=new List<string>();
			}
			// source of the event - change in higher-level model that is not reflected in lower-level model
			// type of item that changed
			// item that changed in the higher-level model and type of change
			// lower-level model affected by this event
			// with this structure, items are grouped chiefly by the higher level model that probably changed, then by they type of item that changed, then by what the change was
			// Once these three things are known, some resolution logic can occur - e.g. if a model has one item that was removed and one item that was added of the same item type,
			// this is probably a simple property rename and it's okay to modify everything downstream by creating the new item, copying the value of the old
			// item to the new item, and then deleting the old item.
			this.data[upstream][subjectType][changeEvent].Add(downstream);
			this.findings=this.findings|finding;
		}
		public void WriteAll(){
			foreach(KeyValuePair<string,Dictionary<string,Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>>> upstream in this.data){
				foreach(KeyValuePair<string,Dictionary<KeyValuePair<string,ValidationFinding>,List<string>>> subjectType in upstream.Value){
					foreach(KeyValuePair<KeyValuePair<string,ValidationFinding>,List<string>> subject in subjectType.Value){
						foreach(string downstream in subject.Value){
							try{
							System.Console.Write((subject.Key.Value&(ValidationFinding.INVALID|ValidationFinding.UNRESOLVED))>0?"!!! ":"    ");
							System.Console.WriteLine(subjectType.Key+" "+subject.Key.Key+": "+upstream.Key+" ==> "+this.getLongTitle(RegisteredObject.GetRegisteredObject(downstream))+" ("+downstream+")");
							}catch{
							}
						}
					}
				}
			}
		}
	}
}

