﻿using System;
using System.IO;
using System.Reflection;
using System.Linq;

namespace Revanche.Lib
{
	public enum ExampleProjects{
		TASK_TRACKER,
		CHARACTER_MANAGER,
		PRODUCT_CATALOG,
	}

	public static class Example
	{
		/// <summary>
		/// Creates the readme file to help people with writing structures.
		/// </summary>
		public static void WriteReadmeFile()
		{
			Example.CopyResourceToStructs("Revanche.Examples.STRUCTURE.md","STRUCTURE.md");
		}

		private static void CopyResourceToStructs(string from,string to)
		{
			string path=Profile.StructFolder+Path.DirectorySeparatorChar+to;
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			using(StreamWriter sw=new StreamWriter(path))
			using(StreamReader sr=new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(from))) {
				sw.Write(sr.ReadToEnd());
			}
		}

		public static void CreateProject(ExampleProjects project){
			string name="EXAMPLE_"+Enum.GetName(typeof(ExampleProjects),project);
			if(!Profile.Profiles.Contains(name)) {
				Profile.Create("EXAMPLE_"+Enum.GetName(typeof(ExampleProjects),project));
				string current=Profile.Current;
				Profile.Load(name);
				switch(project){
					case ExampleProjects.PRODUCT_CATALOG:
						Example.CopyResourceToStructs("Revanche.Examples.types_product.yml","types_product.yml");
						Example.CopyResourceToStructs("Revanche.Examples.models_product.yml","models_product.yml");
						break;
					case ExampleProjects.TASK_TRACKER:
						Example.CopyResourceToStructs("Revanche.Examples.types_task.yml","types_task.yml");
						Example.CopyResourceToStructs("Revanche.Examples.models_task.yml","models_task.yml");
						break;
					case ExampleProjects.CHARACTER_MANAGER:
						Example.CopyResourceToStructs("Revanche.Examples.types_characters.yml","types_characters.yml");
						Example.CopyResourceToStructs("Revanche.Examples.models_characters.yml","models_characters.yml");
						break;
				}
				Profile.Load(current);
			}
		}

		public static bool HasProject(ExampleProjects project){
			return Profile.Profiles.Contains("EXAMPLE_"+Enum.GetName(typeof(ExampleProjects),project));
		}
	}
}

