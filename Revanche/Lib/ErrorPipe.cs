using System;
using System.Collections.Generic;

namespace Revanche.Lib
{
	/// <summary>
	/// During serialization/deserialization procedures, the real error may not be returned to the controller, so this pipe provides access to more useful error handling information
	/// </summary>
	public static class ErrorPipe
	{
		private static List<Exception> data = new List<Exception>();
		public static bool DemandInvalidation{ get; set;}
		public static void Stack(Exception e){
			data.Add(e);
		}
		public static Exception[] Unstack(){
			List<Exception> list=new List<Exception>();
			foreach(Exception e in ErrorPipe.data) {
				Exception f=e;
				while(f!=null) {
					if(!list.Contains(f)) {
						list.Add(f);
					}
					f=f.InnerException;
				}
			}
			Exception[] ret=list.ToArray();
			data.Clear();
			ErrorPipe.DemandInvalidation=false;
			return ret;
		}
	}
}

