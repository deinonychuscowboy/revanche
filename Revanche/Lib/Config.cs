using System;
using Revanche.Lib;
using System.IO;

namespace Revanche.Lib
{
	/// <summary>
	/// Class which maintains user settings. Not stored in the revanche directory, stored in the user's home directory in a dot file.
	/// </summary>
	public static class Config
	{
		private static bool buttonIcons=true;
		private static bool buttonText=false;
		private static string userHome=
			( Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)!=""
				? Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
				: ( (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
					? Environment.GetEnvironmentVariable("HOME")
					: Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%") ) != ""
				? (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
					? Environment.GetEnvironmentVariable("HOME")
					: Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%")
				: Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().CodeBase) )
			+Path.DirectorySeparatorChar;
		private static string root=userHome+"Revanche"+Path.DirectorySeparatorChar;

		public static string ID_PROTOCOL{ get; set; }="rvi";
		public static string TITLE_PROTOCOL{ get; set; }="rvt";
		public static bool COLLAPSE_HIDE_BUTTONS { get; set;}=true;
		public static bool COLLAPSE_START { get; set;}=false;

		/// <summary>
		/// Whether to show button icons in the main view.
		/// </summary>
		/// <value><c>true</c> if BUTTO n_ ICON; otherwise, <c>false</c>.</value>
		public static bool BUTTON_ICONS
		{
			get{
				return Config.buttonIcons;
			}
			set{
				buttonIcons=value;
				if(value==false){
					buttonText=true;
				}
			}
		}

		/// <summary>
		/// Whether to show button text in the main view.
		/// </summary>
		/// <value><c>true</c> if BUTTO n_ TEX; otherwise, <c>false</c>.</value>
		public static bool BUTTON_TEXT
		{
			get{
				return Config.buttonText;
			}
			set{
				buttonText=value;
				if(value==false){
					buttonIcons=true;
				}
			}
		}

		/// <summary>
		/// Root folder for structure, data, and backups.
		/// </summary>
		/// <value>The ROO.</value>
		public static string ROOT
		{
			get{
				return Config.root;
			}
			set{
				if(root!=value){
					// create parents and new directory, then delete new directory to prevent IOException, leaving parents
					Directory.CreateDirectory(value);
					Directory.Delete(value);
					Directory.Move(root,value);
				}
				root=value;
				if(!root.EndsWith(Path.DirectorySeparatorChar+"")){
					root+=Path.DirectorySeparatorChar;
				}
			}
		}

		/// <summary>
		/// What UI style to use to indicate a property has notes. Valid values are 'u' for underline or 'i' for italics.
		/// </summary>
		/// <value>The NOT e_ STYL.</value>
		public static string NOTE_STYLE{get;set;}="blue";

		/// <summary>
		/// How much padding to put between properties in the UI.
		/// </summary>
		/// <value>The PROPERT y_ PADDIN.</value>
		public static uint PROPERTY_PADDING{get;set;}=0u;

		/// <summary>
		/// Date format string.
		/// </summary>
		/// <value>The DAT e_ FORMA.</value>
		public static string DATE_FORMAT{get;set;}="M/d/yyyy";

		/// <summary>
		/// Time format string.
		/// </summary>
		/// <value>The TIM e_ FORMA.</value>
		public static string TIME_FORMAT { get; set;}="h:mm tt";

		/// <summary>
		/// Default profile to load
		/// </summary>
		/// <value></value>
		public static string DEFAULT_PROFILE{get;set;}="default";

		private static bool boolParse(string boolval){
			return boolval.ToLower()=="true"||boolval=="1"||boolval.ToLower().StartsWith("y");
		}

		/// <summary>
		/// Load configuration from the dot file.
		/// </summary>
		public static void Load()
		{
			string configfile=Config.userHome+".revanche";
			if(File.Exists(configfile)){
				using(StreamReader sr=new StreamReader(configfile)){
					string line;
					while((line=sr.ReadLine())!=null){
						if(line!=""){
							string[] config=line.Split('=');
							if(config.Length!=2){
								System.Console.Error.WriteLine("Malformed config option: "+line);
							} else{
								switch(config[0]){
									case "buttonIcons":
										buttonIcons=Boolean.Parse(config[1]);
										break;
									case "buttonText":
										buttonText=Boolean.Parse(config[1]);
										break;
									case "root":
										Directory.CreateDirectory(config[1]);
										root=config[1];
										break;
									case "noteStyle":
										if(config[1]!="i"&&config[1]!="u"&&config[1]!="red"&&config[1]!="blue"&&config[1]!="yellow"&&config[1]!="green"){
											System.Console.Error.WriteLine("Invalid config option: "+line);
											System.Console.Error.WriteLine("Note style must be one of 'i', 'u', 'red', 'green', 'yellow', 'blue'.");
										} else{	
											NOTE_STYLE=config[1];
										}
										break;
									case "propertyPadding":
										PROPERTY_PADDING=UInt32.Parse(config[1]);
										break;
									case "dateFormat":
										DATE_FORMAT=config[1];
										break;
									case "timeFormat":
										TIME_FORMAT=config[1];
										break;
									case "collapseHideButtons":
										COLLAPSE_HIDE_BUTTONS=Config.boolParse(config[1]);
										break;
									case "collapseStart":
										COLLAPSE_START=config[1].ToLower()=="true"||config[1]=="1"||config[1].ToLower().StartsWith("y");
										break;
									case "defaultProfile":
										DEFAULT_PROFILE=config[1];
										break;
									case "idProtocol":
										ID_PROTOCOL=config[1];
										break;
									case "titleProtocol":
										TITLE_PROTOCOL=config[1];
										break;
								}
							}
						}
					}
				}
			} else{
				Config.Save();
			}
		}

		/// <summary>
		/// Save configuration to the dot file.
		/// </summary>
		public static void Save()
		{
			string configfile=userHome+".revanche";
			using(StreamWriter sw=new StreamWriter(configfile,false)){
				sw.WriteLine("buttonIcons="+buttonIcons.ToString());
				sw.WriteLine("buttonText="+buttonText.ToString());
				sw.WriteLine("root="+root.ToString());
				sw.WriteLine("noteStyle="+NOTE_STYLE.ToString());
				sw.WriteLine("propertyPadding="+PROPERTY_PADDING.ToString());
				sw.WriteLine("dateFormat="+DATE_FORMAT.ToString());
				sw.WriteLine("timeFormat="+TIME_FORMAT.ToString());
				sw.WriteLine("collapseHideButtons="+COLLAPSE_HIDE_BUTTONS.ToString());
				sw.WriteLine("collapseStart="+COLLAPSE_START.ToString());
				sw.WriteLine("defaultProfile="+DEFAULT_PROFILE.ToString());
				sw.WriteLine("idProtocol="+ID_PROTOCOL.ToString());
				sw.WriteLine("titleProtocol="+TITLE_PROTOCOL.ToString());
			}
		}
	}
}

