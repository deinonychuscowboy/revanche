using System;
using System.Collections.Generic;
using Revanche.Lib;

namespace Revanche.Lib
{
	/// <summary>
	/// Object which manages its own instance references - going out of scope will not cause the object to be destroyed
	/// </summary>
	public class RegisteredObject
	{
		private static Dictionary<string,RegisteredObject> directory=new Dictionary<string, RegisteredObject>();
		private string id;
		// If this object is a copy of another, the base is here. Data in this object should be MORE accurate than data in the 
		// base object, so there is no need to get the base, but it is important to keep the base around for identity checking.
		private RegisteredObject baseObject;

		/// <summary>
		/// Unique identifier for this object.
		/// </summary>
		/// <value>The identifier.</value>
		public string Id
		{
			get{
				return this.id;
			}
			set{
				if(value!=null&&directory.ContainsKey(value)){
					throw new InvalidOperationException("Object name collision: '"+value+"' is already defined.");
				}
				if(this.id!=null&&directory.ContainsKey(this.id)){
					directory.Remove(this.id);
				}
				this.id=value;
				directory.Add(this.id,this);
			}
		}

		/// <summary>
		/// Remove this instance from the instance registry.
		/// </summary>
		protected void Deregister()
		{
			directory.Remove(this.id);
			this.id=null;
		}

		/// <summary>
		/// Fetch a particular object using its Id attribute.
		/// </summary>
		/// <returns>The registered object.</returns>
		/// <param name="name">Name.</param>
		public static RegisteredObject GetRegisteredObject(string name)
		{
			if(directory.ContainsKey(name)){
				return directory[name];
			} else{
				ArgumentException e=new ArgumentException("Not a registered object: "+name);
				ErrorPipe.Stack(e);
				throw e;
			}
		}

		public static bool HasRegisteredObject(string name){
			return directory.ContainsKey(name);
		}
		
		public static void Clear(){
			RegisteredObject.directory.Clear();
		}

		public RegisteredObject()
		{
		}

		public RegisteredObject(RegisteredObject copy)
		{
			// do NOT use property so directory is not updated - don't want to store this in the directory and accidentally
			// overwrite the base
			this.id=copy.id;
			this.baseObject=copy;
		}

		public RegisteredObject(string name)
		{
			this.Id=name; // Use property so directory is updated
		}

		public override bool Equals(object obj)
		{
			// If this object is a copy, it is considered equal to the object it is based on.
			return base.Equals(obj)||this.baseObject!=null&&this.baseObject.Equals(obj);
		}

		/// <summary>
		/// Execute a delegate over the objects in the registry. Safer than providing access to the registry.
		/// </summary>
		/// <param name="function">Function.</param>
		public static void Iterate(Action<RegisteredObject> function)
		{
			foreach(KeyValuePair<string,RegisteredObject> kvp in directory){
				function(kvp.Value);
			}
		}

		/// <summary>
		/// Gets the registered objects with a specific type
		/// </summary>
		/// <returns>The registered objects.</returns>
		/// <param name="type">Type.</param>
		protected static RegisteredObject[] GetRegisteredObjects(Type type){
			List<RegisteredObject> objects=new List<RegisteredObject>();
			RegisteredObject.Iterate((RegisteredObject obj) =>
			{
				if(obj.GetType()==type||obj.GetType().IsSubclassOf(type)) {
					objects.Add(obj);
				}
			});
			return objects.ToArray();
		}
	}
}

