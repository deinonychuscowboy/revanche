using System;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using Gtk;
using Revanche.Lib;

namespace Revanche
{
	public enum ProgramStage
	{
		STOPPED,
		INIT,
		LOAD,
		VALIDATE,
		APPLICATION,
		BACKUP,
		SAVE
	}

	public class IpcEventArgs:EventArgs{
		public string Cmd{ get; private set; }
		public IpcEventArgs(string cmd){
			this.Cmd=cmd;
		}
	}

	public delegate void IpcEventHandler(object sender,IpcEventArgs e);

	/// <summary>
	/// Main object controlling the program.
	/// </summary>
	public class Controller
	{
		private static string arg;
		private static bool running;
		private static bool terminating;
		private static YamlDataManager ydm;
		public static ProgramStage Stage { get; private set; }
		public static bool Dirty{ get; set; }

		/// <summary>
		/// Nice handler for fatal exceptions.
		/// </summary>
		/// <param name="args">Arguments.</param>
		private static void handleException(GLib.UnhandledExceptionArgs args)
		{
			Exception e=args.ExceptionObject as Exception;
			while(e.InnerException!=null){
				e=e.InnerException;
			}

			// show the error dialog with information about this exception
			Views.Dialogs.Error d=new Views.Dialogs.Error(e,args.ExitApplication||args.IsTerminating);
			d.Modal=true;
			d.Show();
			// run the dialog
			d.Run();

			if(args.ExitApplication||args.IsTerminating) {
				Controller.terminating=true;
			}

			if(args.ExitApplication||args.IsTerminating||ErrorPipe.DemandInvalidation) {
				// quit the event loop to exit as normal
				Application.Quit();
			}
		}

		public static void RequestTermination(){
			Controller.terminating=true;
		}

		public static void Main(string[] args)
		{
			Controller.Stage=ProgramStage.STOPPED;

			if(args.Length>0) {
				Controller.arg=args[0];
			}

			string tmppath=Path.GetTempPath()+"revanche"+Path.DirectorySeparatorChar;
			Directory.CreateDirectory(tmppath);
			string pidfile=tmppath+"revanche.pid";
			string ipcfile=tmppath+"revanche.ipc";
			long bootTime=-1;

			int platform=(int)System.Environment.OSVersion.Platform;
			if(platform==4||platform==128) {
				// running on unix/linux
				bootTime=(long)DateTime.Now.Subtract(DateTime.MinValue).TotalSeconds-(long)double.Parse(File.ReadAllText("/proc/uptime").Split(' ')[0]);
			}
			else{
				using(var uptime=new PerformanceCounter("System","System Up Time")) {
					uptime.NextValue();
					bootTime=(long)DateTime.Now.Subtract(DateTime.MinValue).TotalSeconds-(long)uptime.NextValue();
				}
			}

			// Divide by ten to avoid granularity problems
			bootTime/=10;

			// TODO this needs to be more complex. Revanche should be able to handle maintaining one process for each profile, and opening new profiles in new processes.
			// So, if you have "taco" profile open and click a link for "burrito" profile, you should not lose your "taco" profile instance.
			// If we want to get crazy we can add a button to the profile selector too to launch a new process instead of replacing the current one.
			// would be nice to add a command line arg too, but the arg logic would have to be changed to support those.

			if(File.Exists(pidfile)) {
				// revanche is either running or closed uncleanly
				int pid=-1;
				using(StreamReader sr=new StreamReader(File.OpenRead(pidfile))){
					try{
						string fileBootTime=sr.ReadLine();
						if(fileBootTime==""+bootTime) {
							// the existing pid file appears to be from the same boot time, so the pid number will not have been reused
							pid=int.Parse(sr.ReadLine());
						}
					}catch{}
				}
				if(pid>-1) {
					// we have a possible pid to check from this boot
					Process p=null;
					// TODO if you quit and then immediately run again, this bit gets confused; can we tell if the process is exiting?
					try{
						p=Process.GetProcessById(pid);
					}catch(ArgumentException){}
									if(p!=null) {
						// the process is still running
						System.Console.Write("Revanche is already running: "+pid);
						if(args.Length>0) {
							// we have an arg, so do ipc
							using(StreamWriter sw=new StreamWriter(File.Open(ipcfile,FileMode.Create,FileAccess.Write))) {
								sw.WriteLine(args[0]);
							}
							System.Console.Write(" (IPC Dispatched)");
						}
						// else just exit, no ipc needed
						System.Console.WriteLine();
					} else {
						// the process is no longer running
						Controller.Run(pidfile,ipcfile,bootTime);
					}
				} else {
					// malformed pid file or pid from different boot time, revanche is not running (or we cannot verify that it is)
					Controller.Run(pidfile,ipcfile,bootTime);
				}
			} else {
				// revanche is not running
				Controller.Run(pidfile,ipcfile,bootTime);
			}
		}

		protected static void handleIpc(object sender, FileSystemEventArgs e){
			string cmd=null;
			using(StreamReader sr=new StreamReader(File.OpenRead(e.FullPath))) {
				cmd=sr.ReadLine();
			}
			File.Delete(e.FullPath);
			Gtk.Application.Invoke(null,new IpcEventArgs(cmd),new EventHandler(Controller.consumeIpcCommand));
		}

		protected static void consumeIpcCommand(object sender, EventArgs ea){
			IpcEventArgs e=(IpcEventArgs)ea;
			if(Controller.running) {
				// main window is open and we have a profile already selected
				if(e.Cmd!=null&&Locator.IsSpecifier(e.Cmd)) {
					string profile=Locator.GetProfile(e.Cmd);
					if(profile=="") {
						// assume nothing means current profile... If you use multiple profiles you should probably specify them in the specifier for safety
						profile=Profile.Current;
					}
					if(Profile.Current==profile) {
						// if the profile we want is loaded, handle the specifier normally
						Views.MainWindow.Instance.Current=(Models.Identifiable)Locator.Find(e.Cmd);
					} else {
						// if it is not loaded, set the controller arg and switch profiles (brings controller down and up again to maintain its running status)
						Controller.arg=e.Cmd;
						Controller.SelectProfile(profile);
					}
				} else {
					throw new ArgumentException("IPC command received was not a valid specifier: "+e.Cmd??"null");
				}
			} else {
				// main window is not open, we only have the profile selector up
				string profile=Locator.GetProfile(e.Cmd);
				if(profile=="") {
					// assume nothing means the default profile
					if(Profile.HasDefault) {
						profile=Profile.Default;
					} else {
						throw new ArgumentException("IPC command did not provide a profile, but no default profile exists.");
					}
				}
				// select the profile (does not bring controller up yet since it is not currently running) and set the arg to load
				Controller.SelectProfile(profile);
				Controller.arg=e.Cmd;
				// destroy the profile selector to continue the normal execution pathway.
				// will appear to Run() exactly like the IPC command was given on the command line and we selected the right profile for it from the selector.
				Views.Dialogs.Profile.Instance.Destroy();
			}
		}

		public static void Run(string pidfile, string ipcfile, long bootTime){
			// write pid file
			using(StreamWriter sw=new StreamWriter(File.Open(pidfile,FileMode.Create,FileAccess.Write))){
				sw.WriteLine(bootTime);
				sw.WriteLine(Process.GetCurrentProcess().Id);
			}
			if(File.Exists(ipcfile)) {
				File.Delete(ipcfile);
			}

			FileSystemWatcher ipc=new FileSystemWatcher(Path.GetDirectoryName(ipcfile),"*.ipc");
			ipc.EnableRaisingEvents=true;
			ipc.Created+=Controller.handleIpc;

			// Start Gtk#
			Application.Init();

			// Show exceptions nicely
			GLib.ExceptionManager.UnhandledException+=Controller.handleException;

			bool firstRun=true;
			do {
				Controller.Reset();
				try {
					// load configuration from the dot file
					Config.Load();

					Profile.Setup();

					if(firstRun){
						// if first run and there are args, try to parse them
						if(arg!=null) {
							// we have an arg, so we need to try to load it, but we don't have a profile loaded yet and need that first
							string profile=Locator.GetProfile(arg);
							// try to load the profile if it is provided in the string
							// if not, try a default profile
							if(profile!=""||Profile.HasDefault){
								string toLoad=profile==""?Profile.Default:profile;
								if(!Profile.Profiles.Contains(toLoad)){
									throw new ArgumentException("Profile "+profile+" does not exist.");
								}
								Profile.Load(toLoad);
								// if not specified and no default, let the user choose the profile, and we'll try to process the arg after it loads
							}else{
								Controller.ShowProfileSelector();
							}
							// if first run, no args, and  default is available, try to load it
						}else if(Profile.HasDefault) {
							Profile.Load(Profile.Default);
							// otherwise just display the selector
						}else{
							Controller.ShowProfileSelector();
						}
						// if not first run, just show the selector
					}else{
						Controller.ShowProfileSelector();
					}

					if(Profile.Current!=null) {
						Controller.Startup();
						// show the home screen
						Views.MainWindow.Instance.Current=(firstRun&&arg!=null?Locator.Find(arg):null) as Models.Identifiable;
						Controller.arg=null;

						// Start event loop
						Application.Run();

						Controller.Shutdown();
					}

				} catch(Exception e) {
					Controller.handleException(new GLib.UnhandledExceptionArgs(e,false));
				}

				firstRun=false;
			} while(!Controller.terminating);

			File.Delete(pidfile);
		}

		public static void ShowProfileSelector(){
			new Revanche.Views.Dialogs.Profile(Controller.running);
			Views.Dialogs.Profile.Instance.Run();
			if(Views.Dialogs.Profile.Instance!=null) {
				// dialog may not have been destroyed correctly
				if(Profile.Current==null) {
					// if no profile, the quit signal was probably not sent, so quit in the proper manner for the dialog mode
					Views.Dialogs.Profile.Instance.Quit();
				} else {
					// if there is a profile, we can just destroy the selector.
					Views.Dialogs.Profile.Instance.Destroy();
				}
			} else if(Views.MainWindow.Instance==null) {
				// both instances are null, so quit the application if it's running (this may happen on profile load errors)
				// the run loop will continue as best it has been told after this point
				Application.Quit();
			}
		}

		public static void SelectProfile(string name){
			if(Controller.running){
				Controller.Shutdown();
				Profile.Load(name);
				Controller.Startup();
				Views.MainWindow.Instance.Current=Controller.arg==null?null:(Models.Identifiable)Locator.Find(Controller.arg);
				Controller.arg=null;
			}
			else{
				Profile.Load(name);
			}
		}

		public static bool CreateProfile(string name){
			Profile.Create(name);
			return true;
		}

		public static bool DeleteProfile(string name){
			if(!running||name!=Profile.Current){
				Profile.Delete(name);
				return true;
			} else{
				// TODO we should probably allow this
				Views.Dialogs.Error e=new Revanche.Views.Dialogs.Error("This profile is currently loaded. Please load a different profile before deleting this one.");
				e.Run();
				return false;
			}
		}

		public static bool MoveProfile(string name,string newname){
			if(running&&name==Profile.Current){
				Models.Identifiable entity=Views.MainWindow.Instance.Current;
				Controller.Shutdown();
				Profile.Move(name,newname);
				Profile.Load(newname);
				Controller.Startup();
				Views.MainWindow.Instance.Current=entity;
			} else{
				Profile.Move(name,newname);
			}
			return true;
		}

		public static void Startup()
		{
			Controller.running=true;
			// create the persistence layer and load structure
			Controller.Stage=ProgramStage.INIT;
			Controller.ydm=new Lib.YamlDataManager();
			// check to make sure structure includes the color type
			bool valid=false;
			RegisteredObject.Iterate((RegisteredObject r) => {
				if(r.GetType()==typeof(Types.RevType)&&r.Id=="COLOR"){
					valid=true;
				}
			});
			if(!valid){
				// use default type
				new Types.DefaultColorType();
				valid=true;
			}
			// check to make sure structure includes models
			valid=false;
			RegisteredObject.Iterate((RegisteredObject r) => {
				if(r.GetType()==typeof(Models.RevModel)){
					valid=true;
				}
			});
			if(!valid){
				// no models, probably first run
				throw new NoModelsException();
			}
			// load data
			Controller.Stage=ProgramStage.LOAD;
			Controller.ydm.Load();
			// validate loaded objects against structure
			Controller.Stage=ProgramStage.VALIDATE;
			valid=true;
			bool adds=true;
			List<string> errors=new List<string>();
			while(adds==true&&valid==true){
				adds=false;
				try{
					RegisteredObject.Iterate((RegisteredObject r) => {
						if(r.GetType().GetInterfaces().Contains(typeof(Validatable))){
							ValidationReport report=(r as Validatable).Validate();
							if((report.Findings&ValidationFinding.INVALID)>0){
								// TODO resolution process
								valid=false;
								foreach(string error in report.Data){
									errors.Add(error);
								}
							}
						}
						else{
							#if DEBUG
							System.Console.WriteLine("Not validatable: "+r.Id+" "+r.ToString());
							#endif
						}
					});
				}
				catch(InvalidOperationException){
					// modified registeredobject array while iterating through it, start over
					adds=true;
				}
			}
			if(!valid){
				// some identifiables had data not in their models - probably a rename, alert the user and exit
				throw new RemovedStructuralElementException(String.Join("\n",errors));
			}
			if(ErrorPipe.DemandInvalidation){
				throw new Exception("A deferred error occurred, but no invalid situation was detected.");
			}

			// Create the main window
			Controller.Stage=ProgramStage.APPLICATION;

			// open a fresh window. Window should never be open at this point.
			new Views.MainWindow();

			Views.MainWindow.Instance.Show();
		}

		public static void Shutdown()
		{
			if(Controller.ydm!=null) {
				try {
					if(Profile.Current!=null) {
						// this is a clean exit - backup existing data files
						Controller.Stage=ProgramStage.BACKUP;
						Controller.ydm.Backup();
						// serialize all registered objects to the appropriate locations.
						Controller.Stage=ProgramStage.SAVE;
						Controller.ydm.Save();
					}
				} catch {
					try{
						Controller.ydm.EmgSave();
					}catch{}
				}
			}
			Controller.Reset();
		}

		public static void Reset(){
			Controller.Stage=ProgramStage.STOPPED;
			ErrorPipe.Unstack();
			// immediately close the window if present to avoid outdated data situations
			if(Views.MainWindow.Instance!=null) {
				Views.MainWindow.Instance.Destroy();
			}
			if(Views.Dialogs.Profile.Instance!=null) {
				Views.Dialogs.Profile.Instance.Destroy();
			}
			// TODO we should probably better handle any other windows and dialogs that may be open
			Profile.Unload();
			RegisteredObject.Clear();
			Models.Identifiable.Clear();
			Models.RevModel.Clear();
			Types.RevType.Clear();
			Controller.running=false;
		}
	}

	/// <summary>
	/// Exception thrown when the structure does not contain any RevModel objects.
	/// This means Revanche has no screens to display except the home screen, and the home screen has nothing that could be added to it.
	/// </summary>
	public class NoModelsException : Exception
	{
		public NoModelsException(){
			ErrorPipe.DemandInvalidation=true;
		}
	}

	/// <summary>
	/// Exception thrown when an identifiable has data not in its model.
	/// This means data will not be visible in the UI and is in imminent danger of being lost.
	/// </summary>
	public class RemovedStructuralElementException : Exception
	{
		public RemovedStructuralElementException(string message) : base(message)
		{
			ErrorPipe.DemandInvalidation=true;
		}
	}
}
