using System;

namespace Revanche.Models
{
	public interface Heirarchical
	{
		Heirarchical[] GetParents();

		Heirarchical GetChild(string name);

		Heirarchical[] GetChildren();

		Heirarchical[] GetCollection(string name);

		Heirarchical[][] GetCollections();

		string Title{get;}
	}
}

