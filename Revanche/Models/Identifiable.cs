using System;
using System.Linq;
using System.Collections.Generic;
using Revanche.Types;
using Revanche.Lib;

namespace Revanche.Models
{
	/// <summary>
	/// Main data object. Represents an instance of a RevModel blueprint.
	/// </summary>
	public class Identifiable : RegisteredObject, Validatable, Heirarchical
	{
		/// <remarks>
		/// I'd just like to point out that you were given every opportunity to succeed.
		/// </remarks>
		/// <permission cref="party">
		/// PartyProvider - There was even going to be a party for you.
		/// InvitationProvider - A big party that all your friends were invited to.
		/// ICompanionCube - I invited your best friend, the Companion Cube.
		/// </permission>
		/// <exception>
		/// MurderException - Of course, he couldn't come, because you murdered him.
		/// NoFriendsException - All your other friends couldn't come either, because you don't have any other friends.
		/// UnlikeableException - Because of how unlikeable you are.
		/// </exception>
		/// <see cref="personnel-chell">
		/// It says so here in your personnel file: Unlikeable. Liked by no one. A bitter, unlikeable loner whose passing shall not be mourned.
		/// </see>
		/// <summary>
		/// "Shall not be mourned." That's exactly what it says. Very formal. Very official. 
		/// </summary>
		/// <seealso cref="ibid">
		/// It also says you were adopted. So that's funny too.
		/// </seealso>
		private static List<Identifiable> noParents=new List<Identifiable>();
		private RevModel revModel;
		private Identifiable parent;
		// type information
		private Dictionary<string,Identifiable> children;
		private Dictionary<string,List<Identifiable>> collection;
		private Dictionary<string,object> properties;
		private string notes;
		private Dictionary<string,string> propertyNotes;

		public static new void Clear(){
			Identifiable.noParents.Clear();
		}

		public new string Id
		{
			get {
				return base.Id;
			}
			set {
				Guid g;
				if(!Guid.TryParse(value,out g)) {
					throw new ArgumentException("Invalid object Id: "+value+"\nYour data files may have been corrupted. Try restoring from a backup.");
				}
				base.Id=value;
			}
		}

		/// <summary>
		/// Gets Identifiable instances with no parents.
		/// </summary>
		/// <value>The no parents.</value>
		[YamlDotNet.Serialization.YamlIgnore]
		public static Identifiable[] NoParents
		{
			get {
				return Identifiable.noParents.ToArray();
			}
		}

		/// <summary>
		/// Get an identifiable with a specific Id.
		/// </summary>
		/// <returns>The registered identifiable.</returns>
		/// <param name="id">Identifier.</param>
		public static Identifiable GetRegisteredIdentifiable(string id)
		{
			return Identifiable.GetRegisteredObject(id) as Identifiable;
		}

		/// <summary>
		/// Gets all regisitered identifiables
		/// </summary>
		/// <returns>The registered identifiables.</returns>
		public static Identifiable[] GetRegisteredIdentifiables(){
			return Identifiable.GetRegisteredObjects(typeof(Identifiable)).Select(x=>(Identifiable)x).ToArray();
		}

		/// <summary>
		/// Pretty title for UI display.
		/// </summary>
		/// <value>The title.</value>
		[YamlDotNet.Serialization.YamlIgnore]
		public string Title
		{
			get {
				if(revModel.TitleProp!=null&&properties[revModel.TitleProp]!=null&&properties[revModel.TitleProp].ToString()!="") {
					return properties[revModel.TitleProp].ToString();
				} else
					return (this.parent!=null&&this.parent.GetCollection(this.revModel.Id).Length>0?"Untitled ":"")+this.revModel.Id;
			}
		}

		/// <summary>
		/// Gets or sets the parent object.
		/// </summary>
		/// <value>The parent.</value>
		[YamlDotNet.Serialization.YamlIgnore]
		public Identifiable Parent
		{
			get {
				return this.parent;
			}
			private set {
				if(value!=null&&noParents.Contains(this)) {
					noParents.Remove(this);
				} else if(value==null&&!noParents.Contains(this)) {
					noParents.Add(this);
				}
				parent=value;
			}
		}

		/// <summary>
		/// Gets or sets the RevModel ID, used to interperet the data stored in this object. Used for persistence only.
		/// </summary>
		/// <value>The model.</value>
		public string Model
		{
			get {
				return this.revModel.Id;
			}
			set {
				if(this.revModel!=null) {
					this.revModel.DeregisterImplementation(this);
				}
				this.revModel=RevModel.GetRegisteredModel(value);
				this.revModel.RegisterImplementation(this);
			}
		}

		/// <summary>
		/// Gets or sets the children of this object. Used for persistence only.
		/// </summary>
		/// <value>The children.</value>
		public Dictionary<string, Identifiable> Children
		{
			get {
				return this.children;
			}
			set {
				children=value==null?new Dictionary<string,Identifiable>():value;
				foreach(KeyValuePair<string,Identifiable> i in children) {
					i.Value.Parent=this;
				}
			}
		}

		/// <summary>
		/// Gets or sets the collection objects this object holds. Used for persistence only.
		/// </summary>
		/// <value>The collection.</value>
		public Dictionary<string, List<Identifiable>> Collection
		{
			get {
				return this.collection;
			}
			set {
				collection=value==null?new Dictionary<string,List<Identifiable>>():value;
				foreach(KeyValuePair<string,List<Identifiable>> l in collection) {
					foreach(Identifiable i in l.Value) {
						i.Parent=this;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the properties of this object. Used for persistence only.
		/// </summary>
		/// <value>The properties.</value>
		public Dictionary<string, object> Properties
		{
			get {
				return this.properties;
			}
			set {
				this.properties=this.revModel.CastProperties(value);
			}
		}

		/// <summary>
		/// Gets or sets the global notes for this object.
		/// </summary>
		/// <value>The notes.</value>
		public string Notes
		{
			get {
				return this.notes;
			}
			set {
				notes=value??"";
			}
		}

		/// <summary>
		/// Gets or sets the property notes. Used for persistence only.
		/// </summary>
		/// <value>The property notes.</value>
		public Dictionary<string, string> PropertyNotes
		{
			get {
				return this.propertyNotes;
			}
			set {
				Dictionary<string,string> reinitialized=new Dictionary<string, string>();
				foreach(KeyValuePair<string,string> kvp in value) {
					reinitialized[kvp.Key]=kvp.Value??"";
				}
				propertyNotes=reinitialized;
			}
		}

		/// <summary>
		/// Persistence layer constructor.
		/// </summary>
		public Identifiable()
		{
			noParents.Add(this);
		}

		/// <summary>
		/// Construct a new object.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="model">Model.</param>
		public Identifiable(Identifiable parent,RevModel model)
		{
			Controller.Dirty=true;
			this.revModel=model;
			this.revModel.RegisterImplementation(this);
			this.Id=Guid.NewGuid().ToString(); // use property so directory is updated
			this.Parent=parent; // use property so noParents is updated
			this.notes="";

			this.children=new Dictionary<string, Identifiable>();
			this.collection=new Dictionary<string, List<Identifiable>>();
			this.properties=new Dictionary<string, object>();
			this.propertyNotes=new Dictionary<string, string>();

			foreach(string child in revModel.Children) {
				// create new identifiable objects for the child objects (one-to-one relationships, so create now)
				this.children.Add(child,new Identifiable(this,revModel.GetChild(child) as RevModel));
			}
			foreach(string collection in revModel.Collection) {
				// create new empty lists for the collection objects (one-to-many, so don't create any)
				this.collection.Add(collection,new List<Identifiable>());
			}
			foreach(KeyValuePair<string,string> kvp in revModel.Properties) {
				this.properties.Add(kvp.Key,revModel.GetPropertyType(kvp.Key).DefaultValue);
				this.propertyNotes.Add(kvp.Key,"");
			}

			if(parent!=null&&Array.IndexOf(parent.GetModelInfo().Collection,model.Id)>-1) {
				parent.AddGroupElement(model.Id,this);
			}
		}

		/// <summary>
		/// Compare this object to its model.
		/// Anything in the model, but not in this object, should be added with defaults.
		/// Anything in the object, but not in this model, should be returned as errors for the user to correct.
		/// </summary>
		public ValidationReport Validate()
		{
			ValidationReport report=new ValidationReport();
			// validate that every child specified in the model exists in this object
			foreach(string child in this.revModel.Children) {
				if(!this.children.ContainsKey(child)) {
					// new child added to model, create a default object
					this.children.Add(child,new Identifiable(this,this.revModel.GetChild(child) as RevModel));
					report.AddFinding(ValidationFinding.NEW_FIELD,"Child",child,this.revModel.Id,this.Id);
				}
			}
			// validate that every collection specified in the model exists in this object
			foreach(string collection in this.revModel.Collection) {
				if(!this.collection.ContainsKey(collection)) {
					// new collection added to model, create a default object
					this.collection.Add(collection,new List<Identifiable>());
					report.AddFinding(ValidationFinding.NEW_FIELD,"Collection",collection,this.revModel.Id,this.Id);
				}
			}
			// validate that every property specified in the model exists in this object (with notes)
			foreach(KeyValuePair<string,string> kvp in this.revModel.Properties) {
				bool propertyReported=false;
				if(!this.properties.ContainsKey(kvp.Key)) {
					propertyReported=true;
					// new property added to model, create a default object
					this.properties.Add(kvp.Key,this.revModel.GetPropertyType(kvp.Key).DefaultValue);
					report.AddFinding(ValidationFinding.NEW_FIELD,"Property",kvp.Key,this.revModel.Id,this.Id);
				}
				if(!this.propertyNotes.ContainsKey(kvp.Key)) {
					// new note added to model, create a default object
					this.propertyNotes.Add(kvp.Key,"");
					// only report the note if the property was (somehow) not reported
					if(!propertyReported) {
						report.AddFinding(ValidationFinding.NEW_FIELD,"Note",kvp.Key,this.revModel.Id,this.Id);
					}
				}
			}

			// validate that every child in this object exists in the model
			foreach(string child in this.children.Keys) {
				if(Array.IndexOf<string>(this.revModel.Children,child)==-1) {
					// child in this object but not in model, this is a bad error
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"Child",
						child,
						this.revModel.Id,
						this.Id
					);
				}
			}
			// validate that every collection in this object exists in the model
			foreach(string collection in this.collection.Keys) {
				if(Array.IndexOf<string>(this.revModel.Collection,collection)==-1) {
					// collection in this object but not in model, this is a bad error
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"Collection",
						collection,
						this.revModel.Id,
						this.Id
					);
				}
			}
			List<string> reportedProperties=new List<string>();
			// validate that every property in this object exists in the model
			foreach(string property in this.properties.Keys) {
				if(!this.revModel.Properties.ContainsKey(property)&&this.properties[property]!=null&&(this.properties[property].GetType()!=typeof(string)||(string)this.properties[property]!="")) {
					// property in this object but not in model, this is a bad error
					reportedProperties.Add(property);
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"Property",
						property,
						this.revModel.Id,
						this.Id
					);
				}
			}
			// validate that every property note in this object exists in the model
			foreach(string note in this.propertyNotes.Keys) {
				if(
					// only report note problems if the property problems were not reported
					!reportedProperties.Contains(note)&&!this.revModel.Properties.ContainsKey(note)&&this.propertyNotes[note]!=null&&this.propertyNotes[note]!="") {
					// note in this object but not in model, this is a bad error
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"Note",
						note,
						this.revModel.Id,
						this.Id
					);
				}
			}

			// validate that every property in this object has the correct type
			foreach(KeyValuePair<string,object> kvp in this.properties) {
				try{
					RevType type=this.revModel.GetPropertyType(kvp.Key);
					if(
						(type.ContentType==Content.ENUMERATED||type.ContentType==Content.NULLABLE_ENUMERATED&&kvp.Value!=null)
						&&Array.IndexOf(type.Values,kvp.Value)==-1&&!type.NamedValues.ContainsKey((kvp.Value??"").ToString())) {
						// Invalid property value, this is a bad error
						report.AddFinding(
							ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
							"Value",
							(kvp.Value??"null").ToString(),
							type.Id,
							this.Id
						);
					}
				}catch (ArgumentException e){
					ErrorPipe.Stack(e);
					// no need to do anything with this here since other things will no doubt be wrong
				}
			}

			#if DEBUG
			report.WriteAll();
			#endif

			return report;
		}

		public static bool IsValidId(string id){
			Guid g;
			return Guid.TryParse(id,out g);
		}

		public RevModel GetModelInfo()
		{
			return this.revModel;
		}

		public RevType GetTypeInfo(string propertyName)
		{
			return this.revModel.GetPropertyType(propertyName);
		}

		public Identifiable GetParent()
		{
			return this.parent;
		}

		public Identifiable GetChild(string name)
		{
			return this.children[name];
		}

		public Identifiable[] GetChildren()
		{
			return this.children.Values.ToArray();
		}

		public Identifiable[] GetCollection(string name)
		{
			return this.collection.ContainsKey(name)?this.collection[name].ToArray():new Identifiable[]{ };
		}

		public Identifiable[][] GetCollections()
		{
			return (from List<Identifiable> l in this.collection.Values
			        select l.ToArray()).ToArray();
		}

		Heirarchical[] Heirarchical.GetParents()
		{
			return new Heirarchical[]{ this.GetParent() };
		}

		Heirarchical Heirarchical.GetChild(string name)
		{
			return this.GetChild(name);
		}

		Heirarchical[] Heirarchical.GetChildren()
		{
			return this.GetChildren();
		}

		Heirarchical[] Heirarchical.GetCollection(string name)
		{
			return this.GetCollection(name);
		}

		Heirarchical[][] Heirarchical.GetCollections()
		{
			return this.GetCollections();
		}

		public bool DescendsFrom(Identifiable i){
			Identifiable traverser=this;
			while(traverser!=null) {
				if(traverser==i) {
					return true;
				}
				traverser=traverser.Parent;
			}
			return false;
		}

		public void AddGroupElement(string groupName,Identifiable element)
		{
			this.collection[groupName].Add(element);
		}

		public void RemoveGroupElement(string groupName,Identifiable element)
		{
			if(this.collection.ContainsKey(groupName)&&this.collection[groupName].Contains(element)) {
				this.collection[groupName].Remove(element);
			}
		}

		public object Property(string name)
		{
			return this.properties[name];
		}

		public void SetProperty(string name,object value)
		{
			this.properties[name]=value;
		}

		public string PropertyNote(string name)
		{
			return this.propertyNotes[name];
		}

		public void SetPropertyNote(string name,string value)
		{
			this.propertyNotes[name]=value??"";
		}

		public string HelpNote()
		{
			return this.revModel.HelpNotes;
		}

		public string[] PropertyHelpNote(string name)
		{
			return new string[]{ this.revModel.PropertyHelpNote(name), this.revModel.GetPropertyType(name).HelpNotes };
		}

		public string BuildSpecifierById(){
			return "ID:"+Profile.Current+":/"+this.Id;
		}

		public string BuildSpecifierByTitle(Identifiable relativeTo=null){
			string path="";
			Identifiable traverser=this;
			while(traverser!=relativeTo) {
				path="/"+traverser.Title+path;
				traverser=traverser.Parent;
			}
			return "TITLE:"+Profile.Current+":"+(relativeTo!=null?path.Substring(1):path);
		}

		/// <summary>
		/// Flatten this instance's children for serialization
		/// </summary>
		public List<Identifiable> Flatten()
		{
			List<Identifiable> flattened=new List<Identifiable>();
			flattened.Add(this);
			foreach(KeyValuePair<string,Identifiable> kvp in this.children) {
				foreach(Identifiable child in kvp.Value.Flatten()) {
					flattened.Add(child);
				}
			}
			foreach(KeyValuePair<string,List<Identifiable>> kvp in this.collection) {
				foreach(Identifiable collectionObject in kvp.Value) {
					foreach(Identifiable child in collectionObject.Flatten()) {
						flattened.Add(child);
					}
				}
			}
			return flattened;
		}

		/// <summary>
		/// Delete this instance and all children.
		/// </summary>
		public void Delete()
		{
			List<Identifiable> toDelete=new List<Identifiable>();
			foreach(KeyValuePair<string,Identifiable> kvp in this.children) {
				toDelete.Add(kvp.Value);
			}
			foreach(KeyValuePair<string,List<Identifiable>> kvp in this.collection) {
				foreach(Identifiable collectionObject in kvp.Value) {
					toDelete.Add(collectionObject);
				}
			}
			foreach(Identifiable obj in toDelete) {
				obj.Delete();
			}
			if(this.parent!=null) {
				this.parent.RemoveGroupElement(this.revModel.Id,this);
			} else {
				Identifiable.noParents.Remove(this);
			}
			this.Deregister();
		}

		/// <summary>
		/// Assign this instance to a new parent.
		/// </summary>
		/// <param name="newParent">New parent.</param>
		public void Reparent(Identifiable newParent)
		{
			if(this.parent!=null) {
				this.parent.collection[this.GetModelInfo().Id].Remove(this);
			}

			if(newParent==null) {
				this.Parent=newParent;
			} else {
				if(!this.GetModelInfo().Parents.Contains(newParent.GetModelInfo().Id)) {
					throw new ArgumentException("Object "+newParent+" ("+newParent.GetModelInfo()+") is not a valid parent for "+this+" ("+this.GetModelInfo()+")");
				}

				this.Parent=newParent;
				if(this.parent!=null) {
					this.parent.collection[this.GetModelInfo().Id].Add(this);
				}
			}
		}

		public override string ToString()
		{
			return this.Title;
		}

		public Dictionary<string,object> CalculateTriggers(string key,object value,Dictionary<string,object> currentData=null)
		{
			bool refresh=false;
			Dictionary<string,object> result=new Dictionary<string,object>(currentData??this.properties);
			result[key]=value;
			foreach(Trigger trigger in this.revModel.Triggers) {
				if(trigger.Test(key,value)) {
					foreach(KeyValuePair<string,object> effect in trigger.Effects) {
						result[effect.Key]=effect.Value;
					}
					refresh=true;
				}
			}
			return refresh?result:null;
		}

		public static bool HasRegisteredIdentifiable(string name){
			return Identifiable.HasRegisteredObject(name)&&(Identifiable.GetRegisteredObject(name) as Identifiable)!=null;
		}
	}
}

