using System;
using System.Linq;
using System.Collections.Generic;
using Revanche.Lib;
using System.Text.RegularExpressions;

namespace Revanche.Models
{
	/// <summary>
	/// Primary class which provides a blueprint for Revanche screens and data. A model is a "template" (or "type" in programming terms) which is implemented by an Identifiable.
	/// </summary>
	public class RevModel : RegisteredObject, Validatable, Heirarchical
	{
		private static List<string> collectables=new List<string>();
		private static List<string> childed=new List<string>();
		/// <summary>
		/// I am not a moron.
		/// </summary>
		private static List<string> noParents=new List<string>();
		private static Regex facet=new Regex(@"^[A-Z][a-z]+$");
		// type information
		private List<string> children;
		private List<string> collection;
		private Dictionary<string,string> properties;
		private Dictionary<string,object> defaults;
		private Dictionary<string,bool> showDefaults;
		private string helpNotes;
		private Dictionary<string,string> propertyHelpNotes;
		private List<Trigger> triggers;
		private string titleProp;
		private List<Identifiable> implementations;

		public static new void Clear(){
			RevModel.noParents.Clear();
			RevModel.childed.Clear();
			RevModel.collectables.Clear();
		}

		public new string Id
		{
			get {
				return base.Id;
			}
			set {
				if(!facet.IsMatch(value)) {
					throw new ArgumentException("Invalid model Id: "+value+"\nTypes must have IDs that begin with a capital letter followed by lowercase letters, with no spaces or symbols.");
				}
				base.Id=value;
				if(!RevModel.collectables.Contains(this.Id)&&!RevModel.childed.Contains(this.Id)&&!RevModel.noParents.Contains(this.Id)) {
					RevModel.noParents.Add(this.Id);
				}
			}
		}

		public string Title
		{
			get {
				return this.Id;
			}
		}

		/// <summary>
		/// All models which are somewhere in a collection of something
		/// </summary>
		/// <value>The collectables.</value>
		public static RevModel[] Collectables
		{
			get {
				return (from string d in (from string m in RevModel.collectables
				                          select m).Distinct()
					select RevModel.GetRegisteredModel(d)).ToArray();
			}
		}

		/// <summary>
		/// All models which do not have a parent model
		/// </summary>
		/// <value>The no parents.</value>
		public static RevModel[] NoParents
		{
			get {
				return (from string d in (from string m in RevModel.noParents
				                          select m).Distinct()
				        select RevModel.GetRegisteredModel(d)).ToArray();
			}
		}

		/// <summary>
		/// All models which are somewhere a child of something
		/// </summary>
		/// <value>The childed.</value>
		public static RevModel[] Childed
		{
			get {
				return (from string d in (from string m in RevModel.childed
					select m).Distinct()
					select RevModel.GetRegisteredModel(d)).ToArray();
			}
		}

		/// <summary>
		/// Gets or sets Trigger objects, which store value changes which occur subsequent to another value change.
		/// </summary>
		/// <value>The triggers.</value>
		public Trigger[] Triggers
		{
			get {
				return this.triggers==null?new Trigger[]{ }:this.triggers.ToArray();
			}
			set {
				triggers=new List<Trigger>(value);
			}
		}

		/// <summary>
		/// Gets or sets notes that are stored on the model level and displayed to help the user fill out screens. 
		/// </summary>
		/// <value>The help notes.</value>
		public string HelpNotes
		{
			get {
				return this.helpNotes??"";
			}
			set {
				helpNotes=value;
			}
		}

		/// <summary>
		/// Gets or sets notes that are stored per-property and displayed to help the user fill out fields. Used for persistence only.
		/// </summary>
		/// <value>The property help notes.</value>
		public Dictionary<string, string> PropertyHelpNotes
		{
			get {
				return this.propertyHelpNotes??new Dictionary<string,string>();
			}
			set {
				propertyHelpNotes=value;
			}
		}

		/// <summary>
		/// Gets or sets the default per-property values. Used for persistence only.
		/// </summary>
		/// <value>
		/// The defaults.
		/// </value>
		public Dictionary<string, object> Defaults
		{
			get {
				return this.defaults;
			}
			set {
				this.defaults=this.CastProperties(value);
			}
		}

		/// <summary>
		/// Gets or sets whether to display the per-property default values. Used for persistence only.
		/// </summary>
		/// <value>
		/// The show defaults.
		/// </value>
		public Dictionary<string, bool> ShowDefaults
		{
			get {
				return this.showDefaults;
			}
			set {
				this.showDefaults=value;
			}
		}

		public Dictionary<string,object> CastProperties(Dictionary<string,object> original)
		{
			Dictionary<string,object> casted=new Dictionary<string, object>();
			if(original!=null) {
				foreach(KeyValuePair<string,object> kvp in original) {
					object castedValue=kvp.Value;

					if(this.GetPropertyType(kvp.Key).Id=="DECIMAL"&&castedValue.GetType()!=typeof(double)) {
						castedValue=Double.Parse(kvp.Value.ToString());
					}
					if(this.GetPropertyType(kvp.Key).Id=="INTEGER"&&castedValue.GetType()!=typeof(int)) {
						castedValue=Int32.Parse(kvp.Value.ToString());
					}
					if(this.GetPropertyType(kvp.Key).Id=="BOOLEAN"&&castedValue.GetType()!=typeof(bool)) {
						castedValue=Boolean.Parse(kvp.Value.ToString());
					}
					if(this.GetPropertyType(kvp.Key).Id=="TIMESTAMP"&&castedValue.GetType()!=typeof(long)) {
						castedValue=Int64.Parse(kvp.Value.ToString());
					}
					casted.Add(kvp.Key,castedValue);
				}
			}
			return casted;
		}

		/// <summary>
		/// Gets a model with a specific Id
		/// </summary>
		/// <returns>The registered model.</returns>
		/// <param name="name">Name.</param>
		public static RevModel GetRegisteredModel(string name)
		{
			return RevModel.GetRegisteredObject(name) as RevModel;
		}

		/// <summary>
		/// Gets all registered models
		/// </summary>
		/// <returns>The registered models.</returns>
		public static RevModel[] GetRegisteredModels(){
			RegisteredObject[] objs=RevModel.GetRegisteredObjects(typeof(RevModel));

			return (from RegisteredObject obj in objs
			        select (RevModel)obj).ToArray();
		}

		public Identifiable[] GetImplementations(){
			return this.implementations.ToArray();
		}

		/// <summary>
		/// The property the pretty title should use.
		/// </summary>
		/// <value>The title property.</value>
		public string TitleProp
		{
			get {
				if(this.titleProp!=null) {
					return this.titleProp;
				}else if(properties.ContainsKey("Title")&&properties["Title"]!=null&&properties["Title"].ToString()!="") {
					return "Title";
				} else if(properties.ContainsKey("Name")&&properties["Name"]!=null&&properties["Name"].ToString()!="") {
					return "Name";
				} else
					return null;
			}
			set{
				this.titleProp=value;
			}
		}

		/// <summary>
		/// List of properties which can be used for pretty titles.
		/// </summary>
		/// <value>The title properties.</value>
		[YamlDotNet.Serialization.YamlIgnore]
		public static readonly string[] TitleProps=new string[] { "Title", "Name" };

		public string Parent
		{
			get {
				return null;
			}
			set {
				Exception e= new Exception("Manual setting of the Parent: attribute is now deprecated. Please update your model_ files to remove all usages of the Parent: keyword.");
				ErrorPipe.Stack(e);
				throw e;
			}
		}

		/// <summary>
		/// Model which is the parent of this model, if any.
		/// </summary>
		/// <value>The parent.</value>
		public string[] Parents
		{
			get {
				if(RevModel.noParents.Contains(this.Id)) {
					return new string[]{};
				}else if(RevModel.collectables.Contains(this.Id)||RevModel.childed.Contains(this.Id)){
					List<string> parents=new List<string>();
					RevModel[] models=RevModel.GetRegisteredModels();
					if(RevModel.collectables.Contains(this.Id)){
						foreach(RevModel model in models) {
							if(!parents.Contains(model.Id)&&model.collection.Contains(this.Id)) {
								parents.Add(model.Id);
							}
						}
					}
					if(RevModel.childed.Contains(this.Id)){
						foreach(RevModel model in models) {
							if(!parents.Contains(model.Id)&&model.children.Contains(this.Id)) {
								parents.Add(model.Id);
							}
						}
					}
					return parents.ToArray();
				}
				throw new Exception("Invalid state: "+this.Id+" is not a childed or collectable model, but is not listed as non-parented.");
			}
		}

		/// <summary>
		/// List of models which are children of this model, if any.
		/// </summary>
		/// <value>The children.</value>
		public string[] Children
		{
			get {
				return children.ToArray();
			}
			set {
				this.children=value==null?new List<string>():new List<string>(value);
				foreach(string child in this.children) {
					if(!RevModel.childed.Contains(child)) {
						RevModel.childed.Add(child);
					}
				}
				foreach(string child in this.children) {
					if(RevModel.noParents.Contains(child)) {
						RevModel.noParents.Remove(child);
					}
				}
			}
		}

		/// <summary>
		/// List of model types which this model may collect many of, if any.
		/// </summary>
		/// <value>The collection.</value>
		public string[] Collection
		{
			get {
				return collection.ToArray();
			}
			set {
				this.collection=value==null?new List<string>():new List<string>(value);
				foreach(string collect in this.collection) {
					if(!RevModel.collectables.Contains(collect)) {
						RevModel.collectables.Add(collect);
					}
				}
				foreach(string collect in this.collection) {
					if(RevModel.noParents.Contains(collect)) {
						RevModel.noParents.Remove(collect);
					}
				}
			}
		}

		/// <summary>
		/// List of properties this model has. Key is the property name, value is the RevType Id. Used for persistence only.
		/// </summary>
		/// <value>The properties.</value>
		public Dictionary<string,string> Properties
		{
			get {
				return this.properties;
			}
			set {
				this.properties=value==null?new Dictionary<string,string>():value;
			}
		}

		/// <summary>
		/// Persistence constructor.
		/// </summary>
		public RevModel()
		{
			this.children=new List<string>();
			this.collection=new List<string>();
			this.properties=new Dictionary<string, string>();
			this.propertyHelpNotes=new Dictionary<string, string>();
			this.defaults=new Dictionary<string, object>();
			this.showDefaults=new Dictionary<string, bool>();
			this.triggers=new List<Trigger>();
			this.implementations=new List<Identifiable>();
		}

		private bool invalidValue(object test,Revanche.Types.RevType type)
		{
			// if freeform, always false
			if(type.ContentType==Types.Content.FREEFORM) {
				return false;
			}
			// if numeric, invalid if not a number type
			if(type.ContentType==Types.Content.NUMERIC){
				return !(test is sbyte||test is byte||test is short||test is ushort||test is int||test is uint||test is long||test is ulong||test is float||test is double||test is decimal);
			}
			// if enumerated, invalid if null or not in values array
			if(type.ContentType==Types.Content.ENUMERATED) {
				return test==null||Array.IndexOf(
					type.Values,
					test
				)==-1;
			}
			// if nullable enumerated and not color, invalid if not in values array and not null
			if(type.ContentType==Types.Content.NULLABLE_ENUMERATED&&type.Id!="COLOR") {
				return test!=null&&Array.IndexOf(
					type.Values,
					test
				)==-1;
			}
			// if color, invalid if not in named values keys
			if(type.Id=="COLOR") {
				return test!=null&&!type.NamedValues.Keys.Contains(test);
			}
			return true;
		}

		public ValidationReport Validate()
		{
			ValidationReport report=new ValidationReport();

			// validate that property types exist
			foreach(KeyValuePair<string,string> kvp in this.properties) {
				try {
					if(!kvp.Value.StartsWith("RELATION ")) {
						// do NOT get relation types since that will modify the registered object collection we are currently iterating over
						Types.RevType.GetRegisteredType(kvp.Value);
					}else{
						// instead, try to get any/all types they reference
						foreach(string type in kvp.Value.Split(' ').Skip(1)){
							Types.RevType.GetRegisteredType(type);
						}
					}
				} catch(ArgumentException) {
					report.AddFinding(
						ValidationFinding.REFERENCE|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"UnknownType",
						kvp.Value,
						this.Id,
						this.Id
					);
				}
			}

			// validate that child models exist
			foreach(string child in this.children) {
				try {
					RevModel.GetRegisteredModel(child);
				} catch(ArgumentException) {
					report.AddFinding(
						ValidationFinding.REFERENCE|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"UnknownModel",
						child,
						this.Id,
						this.Id
					);
				}
			}

			// validate that collection models exist
			foreach(string child in this.collection) {
				try {
					RevModel.GetRegisteredModel(child);
				} catch(ArgumentException) {
					report.AddFinding(
						ValidationFinding.REFERENCE|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"UnknownModel",
						child,
						this.Id,
						this.Id
					);
				}
			}

			// validate that relation models exist
			foreach(KeyValuePair<string,string> kvp in this.properties) {
				if(kvp.Value.StartsWith("RELATION ")) {
					string[] models=kvp.Value.Split(' ').Skip(1).ToArray();
					foreach(string model in models) {
						try {
							RevModel.GetRegisteredModel(model);
						} catch(ArgumentException) {
							report.AddFinding(
								ValidationFinding.REFERENCE|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
								"UnknownModel",
								model,
								this.Id,
								this.Id
							);
						}
					}
				}
			}

			// validate that each default corresponds to a property, and is a valid value for that property
			foreach(KeyValuePair<string,object> kvp in this.defaults) {
				if(!this.properties.ContainsKey(kvp.Key)) {
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"Default",
						kvp.Key,
						this.Id,
						this.Id
					);
				} else {
					Types.RevType type=this.GetPropertyType(kvp.Key);
					if(this.invalidValue(kvp.Value,type)) {
						report.AddFinding(
							ValidationFinding.DEFAULT_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
							"DefaultValue",
							(kvp.Value??"null").ToString(),
							type.Id,
							this.Id
						);
					}
				}
			}

			// validate that each showdefault flag corresponds to a property
			foreach(KeyValuePair<string,bool> kvp in this.showDefaults) {
				if(!this.properties.ContainsKey(kvp.Key)) {
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"ShowDefault",
						kvp.Key,
						this.Id,
						this.Id
					);
				}
			}

			// validate that each property note corresponds to a property,.
			foreach(KeyValuePair<string,string> kvp in this.propertyHelpNotes) {
				if(!this.properties.ContainsKey(kvp.Key)) {
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"PropertyHelpNote",
						kvp.Key,
						this.Id,
						this.Id
					);
				}
			}

			foreach(Trigger trig in this.triggers) {
				// validate that each trigger condition corresponds to a property, and is a valid value for that property
				foreach(KeyValuePair<string,object> kvp in trig.Conditions) {
					if(!this.properties.ContainsKey(kvp.Key)) {
						report.AddFinding(
							ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
							"TriggerCondition",
							kvp.Key,
							this.Id,
							this.Id
						);
					} else {
						Types.RevType type=this.GetPropertyType(kvp.Key);
						if(this.invalidValue(kvp.Value,type)) {
							report.AddFinding(
								ValidationFinding.DEFAULT_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
								"TriggerConditionValue",
								kvp.Value.ToString(),
								type.Id,
								this.Id
							);
						}
					}
				}
				// validate that each trigger effect corresponds to a property, and is a valid value for that property
				foreach(KeyValuePair<string,object> kvp in trig.Effects) {
					if(!this.properties.ContainsKey(kvp.Key)) {
						report.AddFinding(
							ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
							"TriggerEffect",
							kvp.Key,
							this.Id,
							this.Id
						);
					} else {
						Types.RevType type=this.GetPropertyType(kvp.Key);
						if(this.invalidValue(kvp.Value,type)) {
							report.AddFinding(
								ValidationFinding.DEFAULT_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
								"TriggerEffectValue",
								kvp.Value.ToString(),
								type.Id,
								this.Id
							);
						}
					}
				}
			}

			#if DEBUG
			report.WriteAll();
			#endif

			return report;
		}

		public RevModel[] GetParents()
		{
			return (from string p in this.Parents select RevModel.GetRegisteredModel(p)).ToArray();
		}

		public static bool IsValidId(string id){
			return facet.IsMatch(id);
		}

		public RevModel GetChild(string name)
		{
			if(children.Contains(name)) {
				return RevModel.GetRegisteredModel(name);
			} else {
				ArgumentException e=new ArgumentException("Not a child model type: "+name);
				ErrorPipe.Stack(e);
				throw e;
			}
		}

		public RevModel[] GetChildren()
		{
			return (from string s in this.children
			        select RevModel.GetRegisteredModel(s)).ToArray();
		}

		public RevModel[] GetCollection(string name)
		{
			if(collection.Contains(name)) {
				return new RevModel[]{ RevModel.GetRegisteredModel(name) };
			} else {
				ArgumentException e=new ArgumentException("Not a collection model type: "+name);
				ErrorPipe.Stack(e);
				throw e;
			}
		}

		public RevModel[][] GetCollections()
		{
			return (from string s in this.collection
			        select new RevModel[]{ RevModel.GetRegisteredModel(s) }).ToArray();
		}

		Heirarchical[] Heirarchical.GetParents()
		{
			return this.GetParents();
		}

		Heirarchical Heirarchical.GetChild(string name)
		{
			return this.GetChild(name);
		}

		Heirarchical[] Heirarchical.GetChildren()
		{
			return this.GetChildren();
		}

		Heirarchical[] Heirarchical.GetCollection(string name)
		{
			return this.GetCollection(name);
		}

		Heirarchical[][] Heirarchical.GetCollections()
		{
			return this.GetCollections();
		}

		/// <summary>
		/// Gets the type of a property of this model.
		/// </summary>
		/// <returns>The property type.</returns>
		/// <param name="name">Name.</param>
		public Revanche.Types.RevType GetPropertyType(string name)
		{
			if(properties.ContainsKey(name)) {
				if(this.defaults.ContainsKey(name)||this.showDefaults.ContainsKey(name)) {
					Types.RevType propertyType=new Types.RevType(Types.RevType.GetRegisteredType(properties[name]));
					if(this.defaults.ContainsKey(name)) {
						propertyType.DefaultValue=this.defaults[name];
					}
					if(this.showDefaults.ContainsKey(name)) {
						propertyType.ShowDefault=this.showDefaults[name];
					}
					return propertyType;
				} else {
					return Types.RevType.GetRegisteredType(properties[name]);
				}
			} else {
				throw new ArgumentException("Not a valid property: "+name);
			}
		}

		public string[] GetPropertyNames()
		{
			return (new List<string>(this.properties.Keys)).ToArray();
		}

		public string PropertyHelpNote(string name)
		{
			return this.PropertyHelpNotes.ContainsKey(name)?this.PropertyHelpNotes[name]:"";
		}

		public override string ToString()
		{
			return "Model <"+this.Id+">";
		}

		public static bool HasRegisteredModel(string name){
			return RevModel.HasRegisteredObject(name)&&(RevModel.GetRegisteredObject(name) as RevModel)!=null;
		}

		public string BuildSpecifier(){
			return "MODEL:"+Profile.Current+":/"+this.Id;
		}

		public void RegisterImplementation(Identifiable i){
			this.implementations.Add(i);
		}
		public void DeregisterImplementation(Identifiable i){
			this.implementations.Remove(i);
		}

		public RevModel[] GetDescendantModels(){
			// extra ToArray calls in here are to materialize Linq queries at specific points; not doing this tends to confuse linq and cause stack overflows since we're manipulating the same enumerables linq is querying on
			IEnumerable<RevModel> objs=Enumerable.Empty<RevModel>();
			IEnumerable<RevModel> generation=this.GetChildren().Union(this.GetCollections().SelectMany(i => i).ToArray());
			bool done=false;
			while(!done) {
				IEnumerable<RevModel> newGeneration=Enumerable.Empty<RevModel>();
				done=true;
				foreach(RevModel r in generation){
					IEnumerable<RevModel> nextData=r.GetChildren().Union(r.GetCollections().SelectMany(i => i).ToArray());
					IEnumerable<RevModel> uniqueNextData=nextData.Where(x => !objs.Contains(x) && !newGeneration.Contains(x));
					if(uniqueNextData.Count()>0||!objs.Contains(r)&&!newGeneration.Contains(r)) {
						done=false;
						newGeneration=newGeneration.Union(uniqueNextData.ToArray());
					}
				}
				objs=objs.Union(generation.ToArray());
				generation=newGeneration;
			}

			return objs.ToArray();
		}
	}
}
