using System;
using System.Collections.Generic;

namespace Revanche
{
	/// <summary>
	/// Class that handles storage of mappings from a condition set to an effect set. No actual kicking-off logic is done in this class, it's just data for the Identifiable to reference when determining its own state.
	/// </summary>
	public class Trigger
	{
		public Dictionary<string,object> Conditions{ get; set; }

		public Dictionary<string,object> Effects{ get; set; }

		public Trigger()
		{
			this.Conditions=new Dictionary<string,object>();
			this.Effects=new Dictionary<string,object>();
		}

		public bool Test(string key,object value)
		{
			return Conditions.ContainsKey(key)&&Conditions[key].Equals(value);
		}
	}
}

