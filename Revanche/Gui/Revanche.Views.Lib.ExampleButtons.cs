﻿using System;

namespace Revanche.Views.Lib
{
	public partial class ExampleButtons
	{
		private global::Gtk.VBox vbox1;

		private global::Gtk.Label label1;

		private global::Gtk.Box hbox1;

		private global::Gtk.Button button1;

		private global::Gtk.Button button2;

		private global::Gtk.Button button3;

		private global::Gtk.HSeparator separator;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget test.ExampleButtons
			global::Stetic.BinContainer.Attach (this);
			this.Name = "Revanche.Views.Lib.ExampleButtons";
			// Container child test.ExampleButtons.Gtk.Container+ContainerChild
			this.vbox1 = new global::Gtk.VBox ();
			this.vbox1.Name = "vbox1";
			this.vbox1.Spacing = 6;
			// Container child vbox1.Gtk.Box+BoxChild
			this.label1 = new global::Gtk.Label ();
			this.label1.Name = "label1";
			this.label1.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Example Profiles</b>");
			this.label1.UseMarkup = true;
			this.vbox1.Add (this.label1);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.vbox1 [this.label1]));
			w1.Position = 0;
			w1.Expand = false;
			w1.Fill = false;

			this.separator=new Gtk.HSeparator();
			this.vbox1.Add (this.separator);
			global::Gtk.Box.BoxChild w9 = ((global::Gtk.Box.BoxChild)(this.vbox1 [this.separator]));
			w9.Position = 1;
			w9.Expand = false;
			w9.Fill = false;

			// Container child vbox1.Gtk.Box+BoxChild
			this.hbox1 = this.Orientation==Gtk.Orientation.Horizontal?new global::Gtk.HBox () as Gtk.Box:new global::Gtk.VBox() as Gtk.Box;
			this.hbox1.Name = "hbox1";
			this.hbox1.Spacing = 6;
			// Container child hbox1.Gtk.Box+BoxChild
			this.button1 = new global::Gtk.Button ();
			this.button1.CanFocus = true;
			this.button1.Name = "button1";
			this.button1.UseUnderline = true;
			this.button1.Label = global::Mono.Unix.Catalog.GetString ("Product Catalog");
			global::Gtk.Image w2 = new global::Gtk.Image ();
			w2.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-execute", global::Gtk.IconSize.Menu);
			this.button1.Image = w2;
			this.hbox1.Add (this.button1);
			global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.button1]));
			w3.Position = 0;
			w3.Expand = true;
			w3.Fill = false;
			// Container child hbox1.Gtk.Box+BoxChild
			this.button2 = new global::Gtk.Button ();
			this.button2.CanFocus = true;
			this.button2.Name = "button2";
			this.button2.UseUnderline = true;
			this.button2.Label = global::Mono.Unix.Catalog.GetString ("Task Tracker");
			global::Gtk.Image w4 = new global::Gtk.Image ();
			w4.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-execute", global::Gtk.IconSize.Menu);
			this.button2.Image = w4;
			this.hbox1.Add (this.button2);
			global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.button2]));
			w5.Position = 1;
			w5.Expand = true;
			w5.Fill = false;
			// Container child hbox1.Gtk.Box+BoxChild
			this.button3 = new global::Gtk.Button ();
			this.button3.CanFocus = true;
			this.button3.Name = "button3";
			this.button3.UseUnderline = true;
			this.button3.Label = global::Mono.Unix.Catalog.GetString ("Character Manager");
			global::Gtk.Image w6 = new global::Gtk.Image ();
			w6.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-execute", global::Gtk.IconSize.Menu);
			this.button3.Image = w6;
			this.hbox1.Add (this.button3);
			global::Gtk.Box.BoxChild w7 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.button3]));
			w7.Position = 2;
			w7.Expand = true;
			w7.Fill = false;
			this.vbox1.Add (this.hbox1);
			global::Gtk.Box.BoxChild w8 = ((global::Gtk.Box.BoxChild)(this.vbox1 [this.hbox1]));
			w8.Position = 2;
			w8.Expand = false;
			w8.Fill = false;
			this.Add (this.vbox1);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.Hide ();

			this.button1.Clicked += new global::System.EventHandler (this.handleCreateProduct);
			this.button2.Clicked += new global::System.EventHandler (this.handleCreateTask);
			this.button3.Clicked += new global::System.EventHandler (this.handleCreateCharacter);
		}
	}
}

