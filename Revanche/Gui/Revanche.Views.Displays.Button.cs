
// This file has been generated by the GUI designer. Do not modify.
namespace Revanche.Views.Displays
{
	public partial class Button
	{
		private global::Gtk.HBox hbox1;
		
		private global::Gtk.Button button;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget Revanche.Views.Displays.Button
			global::Stetic.BinContainer.Attach (this);
			this.Name = "Revanche.Views.Displays.Button";
			// Container child Revanche.Views.Displays.Button.Gtk.Container+ContainerChild
			this.hbox1 = new global::Gtk.HBox ();
			this.hbox1.Name = "hbox1";
			this.hbox1.Spacing = 6;
			// Container child hbox1.Gtk.Box+BoxChild
			this.button = new global::Gtk.Button ();
			this.button.CanFocus = true;
			this.button.Name = "button";
			this.button.UseUnderline = true;
			this.button.Label = global::Mono.Unix.Catalog.GetString ("Go");
			global::Gtk.Image w1 = new global::Gtk.Image ();
			w1.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, Lib.IconSelector.Go, global::Gtk.IconSize.Menu);
			this.button.Image = w1;
			this.hbox1.Add (this.button);
			global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.button]));
			w2.PackType = ((global::Gtk.PackType)(1));
			w2.Position = 0;
			w2.Expand = false;
			w2.Fill = false;
			this.Add (this.hbox1);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.Hide ();
			this.button.Clicked += new global::System.EventHandler (this.handleVisit);
		}
	}
}
