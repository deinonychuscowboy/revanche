﻿using System;

namespace Revanche.Views.Lib
{
	public static class IconSelector
	{
		/// <summary>
		/// Used for the toolbar icon that goes to the home screen
		/// </summary>
		/// <value>The home.</value>
		public static string Home
		{
			get {
				return "gtk-home";
			}
		}

		/// <summary>
		/// Used for the toolbar icon that goes up to the parent object
		/// </summary>
		/// <value>Up.</value>
		public static string Up
		{
			get {
				return "gtk-go-up";
			}
		}

		/// <summary>
		/// Used for the example profiles
		/// </summary>
		/// <value>The help.</value>
		public static string Help{
			get{
				return "gtk-help";
			}
		}

		/// <summary>
		/// Used for the toolbar icon that opens the settings window
		/// </summary>
		/// <value>The preferences.</value>
		public static string Preferences{
			get{
				return "gtk-preferences";
			}
		}

		/// <summary>
		/// Used for the toolbar icon that opens the notes dialog for the currently selected object
		/// </summary>
		/// <value>The notes.</value>
		public static string Notes
		{
			get {
				return "gtk-copy";
			}
		}

		/// <summary>
		/// Used for the toolbar icon that opens the edit dialog for the currently selected object
		/// </summary>
		/// <value>The edit.</value>
		public static string Edit
		{
			get {
				return "gtk-edit";
			}
		}

		/// <summary>
		/// Used primarily for locations where you can add a new identifiable object, like collections
		/// </summary>
		/// <value>The add.</value>
		public static string Add
		{
			get {
				return "gtk-add";
			}
		}

		/// <summary>
		/// Used for the toolbar icon that deletes the current object, and a couple other places
		/// </summary>
		/// <value>The remove.</value>
		public static string Remove
		{
			get {
				return "gtk-remove";
			}
		}

		/// <summary>
		/// Used for Ok buttons.
		/// </summary>
		/// <value>The ok.</value>
		public static string Ok
		{
			get {
				return "gtk-ok";
			}
		}

		/// <summary>
		/// Used for Cancel buttons
		/// </summary>
		/// <value><c>true</c> if cancel; otherwise, <c>false</c>.</value>
		public static string Cancel
		{
			get {
				return "gtk-cancel";
			}
		}

		/// <summary>
		/// Used for quit buttons
		/// </summary>
		/// <value>The quit.</value>
		public static string Quit
		{
			get {
				return "gtk-quit";
			}
		}

		/// <summary>
		/// Used for the toolbar icon that opens the about dialog
		/// </summary>
		/// <value>The about.</value>
		public static string About{
			get{
				return "gtk-about";
			}
		}

		/// <summary>
		/// Used for dialog windows that display general information
		/// </summary>
		/// <value>The dialog info.</value>
		public static string DialogInfo{
			get{
				return "gtk-dialog-info";
			}
		}

		/// <summary>
		/// Used for dialog windows that display warnings
		/// </summary>
		/// <value>The dialog warning.</value>
		public static string DialogWarning{
			get{
				return "gtk-dialog-warning";
			}
		}

		/// <summary>
		/// Used for dialog windows that display errors
		/// </summary>
		/// <value>The dialog error.</value>
		public static string DialogError{
			get{
				return "gtk-dialog-error";
			}
		}

		/// <summary>
		/// Used for the expand state of the expand/collapse button
		/// </summary>
		/// <value>The expand.</value>
		public static string Expand
		{
			get {
				return "gtk-goto-bottom";
			}
		}

		/// <summary>
		/// Used for the collapse state of the expand/collapse button
		/// </summary>
		/// <value>The collapse.</value>
		public static string Collapse
		{
			get {
				return "gtk-goto-top";
			}
		}

		/// <summary>
		/// Used for the buttons that open the siblings view
		/// </summary>
		/// <value>The siblings.</value>
		public static string Siblings
		{
			get {
				return "gtk-unindent";
			}
		}

		/// <summary>
		/// Used for the buttons that open the descendants view (dialog to select type)
		/// </summary>
		/// <value>The descendants.</value>
		public static string Descendants{
			get{
				return "gtk-indent";
			}
		}

		/// <summary>
		/// Used for the toolbar button that opens the profiles dialog
		/// </summary>
		/// <value>The profiles.</value>
		public static string Profiles
		{
			get {
				return "gtk-open";
			}
		}

		/// <summary>
		/// Used for the profile dialog's open button and similar
		/// </summary>
		/// <value>The open.</value>
		public static string Open
		{
			get {
				return "gtk-open";
			}
		}

		/// <summary>
		/// Used for the clear button in the color picker dialog and similar places
		/// </summary>
		/// <value>The clear.</value>
		public static string Clear{
			get{
				return "gtk-clear";
			}
		}

		/// <summary>
		/// Used for link buttons and similar
		/// </summary>
		/// <value>The go.</value>
		public static string Go
		{
			get {
				return "gtk-go-forward";
			}
		}

		/// <summary>
		/// Used for trigger buttons and similar
		/// </summary>
		/// <value>The trigger.</value>
		public static string Trigger
		{
			get {
				return "gtk-execute";
			}
		}

		/// <summary>
		/// Used to open the color picker
		/// </summary>
		/// <value>The color of the pick.</value>
		public static string PickColor{
			get{
				return "gtk-color-picker";
			}
		}

		/// <summary>
		/// Used to open an identifiable object from a parent view
		/// </summary>
		/// <value>The view.</value>
		public static string View
		{
			get {
				return "gtk-find";
			}
		}
	}
}

