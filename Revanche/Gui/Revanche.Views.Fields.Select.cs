
// This file has been generated by the GUI designer. Do not modify.
namespace Revanche.Views.Fields
{
	public partial class Select
	{
		private global::Gtk.VBox vbox2;
		
		private global::Revanche.Views.Base.NonStupidComboBox nonstupidcombobox1;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget Revanche.Views.Fields.Select
			global::Stetic.BinContainer.Attach (this);
			this.Name = "Revanche.Views.Fields.Select";
			// Container child Revanche.Views.Fields.Select.Gtk.Container+ContainerChild
			this.vbox2 = new global::Gtk.VBox ();
			this.vbox2.Name = "vbox2";
			this.vbox2.Spacing = 6;
			// Container child vbox2.Gtk.Box+BoxChild
			this.nonstupidcombobox1 = new Revanche.Views.Base.NonStupidComboBox();
			this.vbox2.Add (this.nonstupidcombobox1);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.nonstupidcombobox1]));
			w1.Position = 0;
			w1.Expand = false;
			w1.Fill = false;
			this.Add (this.vbox2);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.Hide ();
		}
	}
}
