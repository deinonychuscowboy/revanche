
// This file has been generated by the GUI designer. Do not modify.
namespace Revanche.Views.Fields
{
	public partial class Apply
	{
		private global::Gtk.Button button17;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget Revanche.Views.Fields.Apply
			global::Stetic.BinContainer.Attach (this);
			this.Name = "Revanche.Views.Fields.Apply";
			// Container child Revanche.Views.Fields.Apply.Gtk.Container+ContainerChild
			this.button17 = new global::Gtk.Button ();
			this.button17.CanFocus = true;
			this.button17.Name = "button17";
			this.button17.Label="Execute Trigger";
			this.button17.UseUnderline = true;
			global::Gtk.Image w1 = new global::Gtk.Image ();
			w1.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, Lib.IconSelector.Trigger, global::Gtk.IconSize.Menu);
			this.button17.Image = w1;
			this.Add (this.button17);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.Hide ();
			this.button17.Clicked += new global::System.EventHandler (this.handlerRun);
		}
	}
}
