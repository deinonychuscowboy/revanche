using System;
using System.Collections.Generic;
using System.Linq;

namespace Revanche.Types
{
	public class RelationType:RevType
	{
		private string[] modelIds;
		public RelationType(string[] modelIds):base("RELATION"+(modelIds.Length==0?"":" "+String.Join(" ",modelIds)),null,Content.NULLABLE_ENUMERATED,null)
		{
			this.modelIds=modelIds;
		}

		public override object[] Values
		{
			get {
				return this.modelIds.Length>0
					?this.modelIds.SelectMany(s => Models.RevModel.GetRegisteredModel(s).GetImplementations().Select(i=>i.BuildSpecifierById())).ToArray()
					:Models.Identifiable.GetRegisteredIdentifiables().Select(i=>i.BuildSpecifierById()).ToArray();
			}
			set{
				throw new ArgumentException("Unable to assign values to a relation type");
			}
		}
	}
}

