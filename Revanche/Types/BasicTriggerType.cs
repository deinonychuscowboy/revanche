﻿using System;
using System.Collections.Generic;

namespace Revanche.Types
{
	public class BasicTriggerType:RevType
	{
		public BasicTriggerType() : base("TRIGGER","TRIGGER",Content.ENUMERATED,new List<object>(){ "TRIGGER" })
		{
			this.ShowDefault=false; // never show in main screen, only in edit
		}
	}
}

