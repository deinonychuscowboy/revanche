﻿using System;
using System.Collections.Generic;

namespace Revanche.Types
{
	/// <summary>
	/// This type works like a BasicColorType, but is not automatically constructed when the program loads - it is only created if no custom COLOR type is defined in the struct.
	/// </summary>
	public class DefaultColorType:RevType
	{
		public DefaultColorType() : base("COLOR",null,Content.NULLABLE_ENUMERATED,new List<object>{ 3, 3, 3, 3, 3 })
		{
			this.NamedValues=new Dictionary<string, object>();
			this.NamedValues.Add("Black","000000");
			this.NamedValues.Add("Gray","7f7f7f");
			this.NamedValues.Add("White","ffffff");
			this.NamedValues.Add("Red","ff0000");
			this.NamedValues.Add("Orange","ff7f00");
			this.NamedValues.Add("Yellow","ffff00");
			this.NamedValues.Add("Green","00ff00");
			this.NamedValues.Add("Blue","0000ff");
		}
	}
}

