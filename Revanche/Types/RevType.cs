using System;
using System.Linq;
using System.Collections.Generic;
using Revanche.Lib;
using System.Text.RegularExpressions;

namespace Revanche.Types
{
	/// <summary>
	/// Defines content types for UI handling.
	/// </summary>
	public enum Content
	{
		ENUMERATED,
		// all user defined types as well as BOOLEAN
		NULLABLE_ENUMERATED,
		// enumerated but allow null values
		FREEFORM,
		// TEXT type only
		NUMERIC
		// INTEGER, TIMESTAMP, and DECIMAL types only
	}

	/// <summary>
	/// Main class which stores type information for property fields.
	/// </summary>
	public class RevType : RegisteredObject, Validatable
	{
		private static Regex facet=new Regex(@"^([A-Z]+|_)+$");
		private object defaultValue;
		private Content contentType;
		private List<object> values;
		private Dictionary<string,object> namedValues;
		private bool showDefault;
		private string helpNotes;
		private Dictionary<string,string> valueHelpNotes;

		/// <summary>
		/// Types which should not be shown in e.g. tables and such because they never provide any useful textual information
		/// </summary>
		public static string[] MetaTypes=new string[]{"TRIGGER"};

		public static new void Clear(){
			// nothing tracked as singleton
		}

		public new string Id
		{
			get {
				return base.Id;
			}
			set {
				if(!facet.IsMatch(value)) {
					throw new ArgumentException("Invalid type Id: "+value+"\nTypes must have IDs that are all caps, with no spaces or symbols except underscores.");
				}
				base.Id=value;
			}
		}
		
		/// <summary>
		/// Helpful notes about this type, displayed in the edit dialog.
		/// </summary>
		/// <value>
		/// The help notes.
		/// </value>
		public string HelpNotes
		{
			get{
				return this.helpNotes??"";
			}
			set{
				helpNotes=value;
			}
		}
		
		/// <summary>
		/// Helpful notes about each value in this type, displayed in the edit dialog. Used for persistence only.
		/// </summary>
		/// <value>
		/// The value help notes.
		/// </value>
		public Dictionary<string, string> ValueHelpNotes
		{
			get{
				return this.valueHelpNotes??new Dictionary<string, string>();
			}
			set{
				valueHelpNotes=value;
			}
		}
		
		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name='copy'>
		/// Copy.
		/// </param>
		public RevType(RevType copy) : base(copy)
		{
			this.defaultValue=copy.defaultValue;
			this.contentType=copy.contentType;
			if(copy.values!=null){
				this.values=new List<object>();
				foreach(object value in copy.values){
					this.values.Add(value);
				}
			}
			if(copy.namedValues!=null){
				this.namedValues=new Dictionary<string, object>();
				foreach(KeyValuePair<string,object> kvp in copy.namedValues){
					this.namedValues[kvp.Key]=kvp.Value;
				}
			}
			this.helpNotes=copy.helpNotes;
			if(copy.valueHelpNotes!=null){
				this.valueHelpNotes=new Dictionary<string, string>();
				foreach(KeyValuePair<string,string> kvp in copy.valueHelpNotes){
					this.valueHelpNotes[kvp.Key]=kvp.Value;
				}
			}
			this.showDefault=copy.showDefault;
		}

		/// <summary>
		/// Persistence constructor.
		/// </summary>
		public RevType()
		{
			this.values=new List<object>();
			this.valueHelpNotes=new Dictionary<string, string>();
			this.showDefault=true;
		}

		/// <summary>
		/// Construct type object. Used only for system types.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="defaultValue">Default value.</param>
		/// <param name="contentType">Content type.</param>
		/// <param name="values">Values.</param>
		protected RevType(string name,object defaultValue,Content contentType,List<object> values) : base(name)
		{
			this.valueHelpNotes=new Dictionary<string, string>();
			this.DefaultValue=defaultValue;
			this.contentType=contentType;
			this.values=values;
			this.showDefault=true;
			if((defaultValue==null||defaultValue.ToString()=="")&&contentType==Content.ENUMERATED){
				throw new Exception("Enumerated content types require a default: "+name);
			}
		}

		public static bool IsValidId(string id){
			return facet.IsMatch(id);
		}

		/// <summary>
		/// Initializes built-in types.
		/// </summary>
		public static void SetupBasicTypes()
		{
			new BasicBooleanType();
			new BasicDecimalType();
			new BasicIntegerType();
			new BasicLinkType();
			new BasicTextType();
			new BasicTimestampType();
			new BasicTriggerType();
			// untyped relation
			new RelationType(new string[]{});
		}

		/// <summary>
		/// Gets a type with a specific Id
		/// </summary>
		/// <returns>The registered type.</returns>
		/// <param name="name">Name.</param>
		public static RevType GetRegisteredType(string name)
		{
			// complex relational types
			if(name.StartsWith("RELATION ")&&!RevType.HasRegisteredType(name)) {
				string[] models=name.Split(' ').Skip(1).ToArray();
				foreach(string model in models) {
					if(!Models.RevModel.HasRegisteredModel(model)) {
						throw new ArgumentException("Invalid relational type: "+name);
					}
				}
				new RelationType(models);
			}

			return RevType.GetRegisteredObject(name) as RevType;
		}

		public static RevType[] GetRegisteredTypes(){
			RegisteredObject[] objs = RevType.GetRegisteredObjects(typeof(RevType));

			return (from RegisteredObject obj in objs
			        select (RevType)obj).ToArray();
		}

		/// <summary>
		/// Default value for this type. Usually required to be set.
		/// </summary>
		/// <value>The default value.</value>
		public object DefaultValue
		{
			get{
				return this.defaultValue;
			}
			set{
				// if the default value is explicitly set to null, the user means this should be a nullable type.
				if(value==null) {
					this.contentType=Content.NULLABLE_ENUMERATED;
				}
				defaultValue=value;
			}
		}
		
		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="Revanche.Types.RevType"/> should show its default value in the UI.
		/// </summary>
		/// <value>
		/// <c>true</c> if show default; otherwise, <c>false</c>.
		/// </value>
		public bool ShowDefault
		{
			get{
				return this.showDefault;
			}
			set{
				this.showDefault=value;
			}
		}

		/// <summary>
		/// Content type of this type (usually Enumerated)
		/// </summary>
		/// <value>The type of the content.</value>
		[YamlDotNet.Serialization.YamlIgnore]
		public Content ContentType
		{
			get{
				return this.contentType;
			}
			set{
				contentType=value;
			}
		}

		/// <summary>
		/// Values this type may take.
		/// </summary>
		/// <value>The values.</value>
		public virtual object[] Values
		{
			get{
				return this.values==null?new object[]{ }:this.values.ToArray();
			}
			set{
				object[] nonNullValues=value.Where(x => x!=null).ToArray();

				// if the user provides an explicit null value, they mean they want this object to be nullable
				if(nonNullValues.Length<value.Length) {
					this.contentType=Content.NULLABLE_ENUMERATED;
				}

				this.values=new List<object>(nonNullValues);
			}
		}

		/// <summary>
		/// In the rare instance that values must have a UI value which corresponds to a separate backend value,
		/// values go in here. Currently only used for COLOR, probably would not be useful unless you plan to
		/// hack on the source code. Should probably be refactored so that regular values use this too just so
		/// everything works the same. Used for persistence only.
		/// </summary>
		/// <value>The named values.</value>
		public virtual Dictionary<string,object> NamedValues
		{
			get{
				return this.namedValues==null?new Dictionary<string,object>():this.namedValues;
			}
			set{
				this.namedValues=value;
			}
		}
		
		/// <summary>
		/// Gets the value with the given name, for named values.
		/// </summary>
		/// <returns>
		/// The named value.
		/// </returns>
		/// <param name='name'>
		/// Name.
		/// </param>
		public object GetNamedValue(string name)
		{
			return this.namedValues[name];
		}
		
		/// <summary>
		/// Gets the value names, for named values.
		/// </summary>
		/// <returns>
		/// The value names.
		/// </returns>
		public string[] GetValueNames()
		{
			return this.namedValues.Keys.ToArray();
		}


		/// <summary>
		/// Validate that the type is defined correctly
		/// </summary>
		public ValidationReport Validate()
		{
			ValidationReport report=new ValidationReport();

			// Check to make sure NamedValues is not used on a type that is not COLOR; the usage has been deprecated
			if(this.NamedValues.Count>0&&this.Id!="COLOR"){
				report.AddFinding(
					ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
					"NamedValues",
					this.defaultValue.ToString(),
					this.Id,
					"COLOR"
				);
			}

			// Check to make sure COLOR has a NamedValues
			if(this.NamedValues.Count==0&&this.Id=="COLOR"){
				report.AddFinding(
					ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
					"NamedValues",
					this.defaultValue.ToString(),
					this.Id,
					this.Id
				);
			}

			// Check to make sure the default value is defined as a value
			if(
				this.ContentType==Content.ENUMERATED&&this.defaultValue==null||
				(this.ContentType==Content.ENUMERATED||this.ContentType==Content.NULLABLE_ENUMERATED&&this.defaultValue!=null)
				&&Array.IndexOf(this.Values,this.defaultValue)==-1&&!this.NamedValues.ContainsKey(this.defaultValue.ToString())){
				report.AddFinding(
					ValidationFinding.DEFAULT_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
					"Value",
					this.defaultValue.ToString(),
					this.Id,
					this.Id
				);
			}

			// validate that each value note corresponds to a value
			foreach(KeyValuePair<string,string> kvp in this.valueHelpNotes){
				if(!this.values.Contains(kvp.Key)){
					report.AddFinding(
						ValidationFinding.DELETED_FIELD|ValidationFinding.INVALID|ValidationFinding.UNRESOLVED,
						"ValueHelpNote",
						kvp.Key,
						this.Id,
						this.Id
					);
				}
			}

			#if DEBUG
			report.WriteAll();
			#endif

			return report;
		}
		
		/// <summary>
		/// Gets the help note for a given value.
		/// </summary>
		/// <returns>
		/// The help note.
		/// </returns>
		/// <param name='name'>
		/// Name.
		/// </param>
		public string ValueHelpNote(string name)
		{
			return this.ValueHelpNotes.ContainsKey(name)?this.ValueHelpNotes[name]:"";
		}
		
		/// <summary>
		/// Gets the help notes for all values.
		/// </summary>
		/// <returns>
		/// The help note.
		/// </returns>
		public string[] ValuesHelpNote()
		{
			if(this.ValueHelpNotes.Count>0){
				List<string> notes=new List<string>();
				foreach(object value in this.values){
					if(this.ValueHelpNotes.ContainsKey(value.ToString())){
						notes.Add("\u2022 "+value.ToString()+": "+this.ValueHelpNotes[value.ToString()]);
					}
				}
				return notes.ToArray();
			} else{
				return new string[]{ };
			}
		}

		public override string ToString()
		{
			return "Type <"+this.Id+">";
		}

		public static bool HasRegisteredType(string name){
			return RevType.HasRegisteredObject(name)&&(RevType.GetRegisteredObject(name) as RevType)!=null;
		}
	}
}

