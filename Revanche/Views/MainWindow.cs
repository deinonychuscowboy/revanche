using System;
using Gtk;
using System.Reflection;
using Revanche.Lib;
using System.Linq;
using System.Collections.Generic;

namespace Revanche.Views
{
	/// <summary>
	/// The main Revanche window. Toolbar with navigation, actions for the current main view item, about dialog, and settings dialog.
	/// </summary>
	public partial class MainWindow: Gtk.Window
	{
		/// <summary>
		/// Get the single instance of MainWindow from elsewhere in the program.
		/// </summary>
		public static MainWindow Instance;
		private string baseTitle;

		private Models.Identifiable current;

		public Models.Identifiable Current
		{
			get {
				return this.current;
			}
			set {
				this.current=value;
				this.RefreshTitle(this.current);
				if(this.mainView.Child!=null) {
					this.mainView.Remove(this.mainView.Child);
				}
				Gtk.Widget view;
				if(this.current==null) {
					Objects.Collection c=new Objects.Collection();
					c.Initialize(Models.Identifiable.NoParents);
					view=c;
					(this.toolbar1.GetNthItem(0) as ToolButton).Sensitive=false;
					(this.toolbar1.GetNthItem(1) as ToolButton).Sensitive=false;
					(this.toolbar1.GetNthItem(3) as ToolButton).Sensitive=false;
					(this.toolbar1.GetNthItem(4) as ToolButton).Sensitive=false;
					(this.toolbar1.GetNthItem(5) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(7) as ToolButton).Sensitive=false;
					(this.toolbar1.GetNthItem(8) as ToolButton).Sensitive=false;
					(this.toolbar1.GetNthItem(9) as ToolButton).Sensitive=false;
				} else {
					view=new Views.Objects.Identifiable(this.current,null);
					if(Config.COLLAPSE_START) {
						(view as Views.Objects.Identifiable).CollapseChildren();
					} else {
						(view as Views.Objects.Identifiable).ExpandChildren();
					}
					(this.toolbar1.GetNthItem(0) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(1) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(3) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(4) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(5) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(7) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(8) as ToolButton).Sensitive=true;
					(this.toolbar1.GetNthItem(9) as ToolButton).Sensitive=true;
				}
				this.mainView.Child=view;
				this.mainView.Child.Show();
				this.RefreshExpandCollapseButton();
			}
		}
				

		/// <summary>
		/// MainWindow should only be constructed once.
		/// </summary>
		public MainWindow() : base(Gtk.WindowType.Toplevel)
		{
			if(Instance!=null){
				throw new InvalidOperationException();
			}
			Instance=this;
			Build();
			AssemblyName name=Assembly.GetExecutingAssembly().GetName();
			this.Title="Revanche "+name.Version.Major+"."+name.Version.Minor;
			#if DEBUG
			this.Title+="."+name.Version.Build+"."+name.Version.Revision;
			#endif
			baseTitle=this.Title;
			this.profileAction.Tooltip="Current Profile: "+Revanche.Lib.Profile.Current;
			this.RefreshViewLayout();
		}

		public override void Destroy(){
			MainWindow.Instance=null;
			base.Destroy();
		}

		/// <summary>
		/// Show all Identifiables of a particular model.
		/// </summary>
		/// <param name="root">Root.</param>
		public void ShowModel(Models.RevModel model,Models.Identifiable parent=null)
		{
			this.RefreshTitle(parent,model);
			if(this.mainView.Child!=null){
				this.mainView.Remove(this.mainView.Child);
			}
			Gtk.Widget view;
			Objects.ModelTreeView mtv=new Objects.ModelTreeView();
			mtv.Initialize(new IdentifiableSet(model,parent));
			view=mtv;
			this.mainView.Child=view;
			this.mainView.Child.Show();

			(this.toolbar1.GetNthItem(0) as ToolButton).Sensitive=true;
			(this.toolbar1.GetNthItem(1) as ToolButton).Sensitive=parent!=null;
			(this.toolbar1.GetNthItem(3) as ToolButton).Sensitive=false;
			(this.toolbar1.GetNthItem(4) as ToolButton).Sensitive=false;
			(this.toolbar1.GetNthItem(5) as ToolButton).Sensitive=false;
			(this.toolbar1.GetNthItem(7) as ToolButton).Sensitive=false;
			(this.toolbar1.GetNthItem(8) as ToolButton).Sensitive=false;
			(this.toolbar1.GetNthItem(9) as ToolButton).Sensitive=false;
		}

		/// <summary>
		/// Refresh the window title with information about the current root object.
		/// </summary>
		/// <param name="root">Root.</param>
		public void RefreshTitle(Models.Identifiable root=null,Models.RevModel model=null)
		{
			string titleString="";
			if(root!=null) {
				titleString=root.Title;
				Models.Identifiable iter=root.Parent;
				while(iter!=null) {
					titleString=iter.Title+"/"+titleString;
					iter=iter.Parent;
				}
				titleString=root.GetModelInfo().Id+": "+titleString;
				if(model!=null) {
					titleString=model.Id+" Objects in "+titleString;
				}
			} else if(model!=null) {
				titleString="All "+model.Id+" Objects";
			}

			if(titleString!=""){
				titleString="     "+titleString;
			}

			this.Title=this.baseTitle+titleString;
		}

		/// <summary>
		/// Refresh all the UI components in the current view.
		/// </summary>
		public void RefreshView()
		{
			if(this.mainView.Child!=null) {
				if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)) {
					(this.mainView.Child as Views.Objects.Identifiable).Refresh();
				} else if(this.mainView.Child.GetType()==typeof(Views.Objects.Collection)) {
					(this.mainView.Child as Views.Objects.Collection).Refresh();
				}
			}
			this.RefreshExpandCollapseButton();
		}

		/// <summary>
		/// Refresh only the layouts of all UI components in the current view.
		/// </summary>
		public void RefreshViewLayout()
		{
			if(this.mainView.Child!=null) {
				if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)) {
					(this.mainView.Child as Views.Objects.Identifiable).RefreshLayout();
				} else if(this.mainView.Child.GetType()==typeof(Views.Objects.Collection)) {
					(this.mainView.Child as Views.Objects.Collection).RefreshLayout();
				}
			}

			this.toolbar1.ToolbarStyle=Revanche.Lib.Config.BUTTON_ICONS
				?Revanche.Lib.Config.BUTTON_TEXT
				?Gtk.ToolbarStyle.Both
				:Gtk.ToolbarStyle.Icons
				:Gtk.ToolbarStyle.Text;
			this.toolbar2.ToolbarStyle=Revanche.Lib.Config.BUTTON_ICONS
				?Revanche.Lib.Config.BUTTON_TEXT
				?Gtk.ToolbarStyle.Both
				:Gtk.ToolbarStyle.Icons
				:Gtk.ToolbarStyle.Text;
		}

		public void RefreshExpandCollapseButton(){
			if(this.mainView.Child!=null&&this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)) {
				this.expandCollapseAction.IconName
					=this.expandCollapseAction.StockId
					=(this.mainView.Child as Views.Objects.Identifiable).ChildrenCollapsed
						?Lib.IconSelector.Expand
						:Lib.IconSelector.Collapse;

				this.expandCollapseAction.Label
					=this.expandCollapseAction.ShortLabel
					=(this.mainView.Child as Views.Objects.Identifiable).ChildrenCollapsed
						?"Expand"
						:"Collapse";

				this.expandCollapseAction.Tooltip=(this.mainView.Child as Views.Objects.Identifiable).ChildrenCollapsed
					?"Expand Children"
					:"Collapse Children";

				ToolItem item=this.toolbar1.GetNthItem(3);
			}
		}

		public void ShowNonFatalError(Exception e){
			Dialogs.Error d=new Dialogs.Error(e,false);
			d.Run();
		}

		/// <summary>
		/// Create a new identifiable object
		/// </summary>
		public void CreateObject(Models.Identifiable parent,Models.RevModel model){
			if(model!=null) {
				Models.Identifiable newChild=new Models.Identifiable(parent,model);
				this.Current=newChild;
				this.Edit();
			} else {
				IEnumerable<Models.RevModel> models;
				if(parent==null) {
					models=Models.RevModel.Collectables.Union(Models.RevModel.NoParents);
				} else {
					models=(parent.GetModelInfo().Collection as IEnumerable<string>).Select(x => Models.RevModel.GetRegisteredModel(x));
				}
				Dialogs.ModelSelector c=new Dialogs.ModelSelector(models.ToArray());
				c.Modal=true;
				c.Show();
				c.Run();
				if(c.Selected!=null) {
					Models.Identifiable newChild=new Models.Identifiable(parent,c.Selected);
					this.Current=newChild;
					this.Edit();
				}
			}
		}

		protected void handleDeleteEvent(object sender,DeleteEventArgs a)
		{
			Controller.RequestTermination();
			Application.Quit();
			a.RetVal=true;
		}

		protected void handlerHome(object sender,EventArgs e)
		{
			this.Current=null;
		}

		protected void handlerUp(object sender,EventArgs e)
		{
			if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)) {
				(this.mainView.Child as Views.Objects.Identifiable).Up(sender,e);
			} else if(this.mainView.Child.GetType()==typeof(Views.Objects.ModelTreeView)) {
				// may be null
				this.Current=(this.mainView.Child as Views.Objects.ModelTreeView).DescendsFrom;
			}else{
				this.Current=null;
			}
		}

		protected void handlerSettings(object sender,EventArgs e)
		{
			Dialogs.Settings s=new Dialogs.Settings();
			s.Modal=true;
			s.Run();
			this.RefreshView();
		}

		protected void handleNotes(object sender,EventArgs e)
		{
			if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)){
				(this.mainView.Child as Objects.Identifiable).Notes(sender,e);
			}
		}

		public void Edit()
		{
			this.handleEdit(this,EventArgs.Empty);
		}

		protected void handleEdit(object sender,EventArgs e)
		{
			if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)){
				(this.mainView.Child as Objects.Identifiable).Edit(sender,e);
			}
		}

		protected void handlerDelete(object sender,EventArgs e)
		{
			if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)){
				(this.mainView.Child as Objects.Identifiable).DeleteModel(sender,e);
			}
		}

		protected void handlerAbout(object sender,EventArgs e)
		{
			Dialogs.About a=new Dialogs.About();
			a.Show();
		}

		protected void handlerSizeRequest(object o,SizeRequestedArgs args)
		{
			this.RefreshViewLayout();
		}

		protected void handleCollapse(object sender,EventArgs e)
		{
			if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)){
				(this.mainView.Child as Objects.Identifiable).ToggleChildCollapseState();
			}
			this.RefreshExpandCollapseButton();
		}

		protected void handleSiblings(object sender,EventArgs e)
		{
			if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)){
				(this.mainView.Child as Objects.Identifiable).ShowSiblings();
			}
		}

		protected void handleDescendants(object sender,EventArgs e)
		{
			if(this.mainView.Child.GetType()==typeof(Views.Objects.Identifiable)) {
				(this.mainView.Child as Objects.Identifiable).ShowDescendants();
			} else {
				Dialogs.ModelSelector d=new Dialogs.ModelSelector( Models.RevModel.GetRegisteredModels(), false );
				d.Modal=true;
				d.Show();
				d.Run();
				Models.RevModel model=d.Selected;
				if(model!=null) {
					this.ShowModel( model, null );
				}
			}
				
		}
		protected void handlerOpen (object sender, EventArgs e)
		{
			Controller.ShowProfileSelector();
		}
	}
}
