using System;
using Revanche.Lib;
using System.Collections.Generic;
using System.Linq;

namespace Revanche.Views.Objects
{
	/// <summary>
	/// UI component which renders an Identifiable. Creates a collapsible frame with title and button bar, then renders the properties, children, and collections of the Identifiable.
	/// Button options allow the user to refocus the main view onto this identifiable, view and edit notes, or edit the properties.
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class Identifiable : Gtk.Bin
	{
		private Models.Identifiable identifiable;
		private Views.Objects.Identifiable parent;
		private Dialogs.Edit editd;
		private Dialogs.Note noted;

		public bool ChildrenCollapsed
		{
			get {
				return this.children.Collapsed;
			}
		}

		public string Title
		{
			get {
				return (parent!=null?parent.Title+"/":"")+this.identifiable.Title;
			}
		}

		public bool Collapsed
		{
			get {
				return this.arrow.ArrowType==Gtk.ArrowType.Right;
			}
		}

		public Identifiable(Models.Identifiable identifiable,Views.Objects.Identifiable parent)
		{
			this.parent=parent;
			this.identifiable=identifiable;
			this.Build();
			this.properties.Initialize(identifiable);
			this.children.Initialize(identifiable,this);
			this.collection.Initialize(identifiable);
			if(parent==null) {
				toolbar.Hide();
				separator.Hide();
				rootAlignment.TopPadding=0;
				containerFrame.ShadowType=Gtk.ShadowType.None;
			}
			this.Refresh();
		}

		public void Collapse()
		{
			this.arrow.ArrowType=Gtk.ArrowType.Right;
			this.dataContainer.Hide();
			this.separator.Hide();
			if(Config.COLLAPSE_HIDE_BUTTONS) {
				this.actionButtons.Hide();
			}
		}

		public void Expand()
		{
			this.arrow.ArrowType=Gtk.ArrowType.Down;
			this.dataContainer.Show();
			this.separator.Show();
			this.actionButtons.Show();
		}

		public void CollapseAll()
		{
			this.CollapseChildren();
			this.Collapse();
		}

		public void ExpandAll()
		{
			this.ExpandChildren();
			this.Expand();
		}

		protected void handlerArrowToggle(object o,Gtk.ButtonReleaseEventArgs args)
		{
			if(!this.Collapsed) {
				Collapse();
			} else {
				Expand();
			}
		}

		public void Refresh()
		{
			this.title.LabelProp=this.identifiable.Title;
			if(parent==null) {
				MainWindow.Instance.RefreshTitle(this.identifiable);
			}
			if(Config.BUTTON_ICONS) {
				this.notesIcon.Show();
				this.editIcon.Show();
				this.viewIcon.Show();
				this.collapseIcon.Show();
				this.siblingIcon.Show();
				this.descendantsIcon.Show();
			} else {
				this.notesIcon.Hide();
				this.editIcon.Hide();
				this.viewIcon.Hide();
				this.collapseIcon.Hide();
				this.siblingIcon.Hide();
				this.descendantsIcon.Hide();
			}
			if(Config.BUTTON_TEXT) {
				this.notesLabel.Show();
				this.editLabel.Show();
				this.viewLabel.Show();
				this.collapseLabel.Show();
				this.siblingLabel.Show();
				this.descendantsLabel.Show();
			} else {
				this.notesLabel.Hide();
				this.editLabel.Hide();
				this.viewLabel.Hide();
				this.collapseLabel.Hide();
				this.siblingLabel.Hide();
				this.descendantsLabel.Hide();
			}
			this.notes.LabelProp=identifiable.Notes;
			if(this.notes.LabelProp=="") {
				this.alignment6.BottomPadding=0;
			} else {
				this.alignment6.BottomPadding=10;
			}
			this.properties.Refresh();
			this.children.Refresh();
			this.collection.Refresh();
			RefreshLayout();
		}

		public void RefreshLayout()
		{
			int mainWindowWidth;
			int mainWindowHeight;
			MainWindow.Instance.GetSize(out mainWindowWidth,out mainWindowHeight);
			this.notes.WidthRequest=mainWindowWidth*3/4;
			this.properties.RefreshLayout();
			this.children.RefreshLayout();
			this.collection.RefreshLayout();
		}

		public void Up(object sender,EventArgs e)
		{
			MainWindow.Instance.Current=this.identifiable.Parent;
		}

		public void Edit(object sender,EventArgs e)
		{
			this.handlerEdit(sender,e);
		}

		public void Notes(object sender,EventArgs e)
		{
			this.handlerNotes(sender,e);
		}

		public void ToggleChildCollapseState()
		{
			if(!this.children.Collapsed) {
				this.CollapseChildren();
			} else {
				this.ExpandChildren();
			}
		}

		public void CollapseChildren()
		{
			if(this.Collapsed) {
				this.Expand();
			}
			this.collapseIcon.SetFromStock(Lib.IconSelector.Expand,Gtk.IconSize.Menu);
			this.collapseLabel.LabelProp="Expand";
			if(!Config.BUTTON_TEXT) {
				this.collapseButton.TooltipText=this.collapseLabel.LabelProp;
			}
			this.children.CollapseAll();
		}

		public void ExpandChildren()
		{
			if(this.Collapsed) {
				this.Expand();
			}
			this.collapseIcon.SetFromStock(Lib.IconSelector.Collapse,Gtk.IconSize.Menu);
			this.collapseLabel.LabelProp="Collapse";
			if(!Config.BUTTON_TEXT) {
				this.collapseButton.TooltipText=this.collapseLabel.LabelProp;
			}
			this.children.ExpandAll();
		}

		public void DeleteModel(object sender,EventArgs e)
		{
			Dialogs.Delete d=new Dialogs.Delete(this.identifiable);
			d.Modal=true;
			d.Run();
		}

		public void ShowSiblings(){
			Models.RevModel model=this.identifiable.GetModelInfo();
			MainWindow.Instance.ShowModel(model,null);
		}

		public void ShowDescendants(){
			Dialogs.ModelSelector d=new Dialogs.ModelSelector(this.identifiable.GetModelInfo().GetDescendantModels().ToArray(),false);
			d.Modal=true;
			d.Show();
			d.Run();
			Models.RevModel model=d.Selected;
			if(model!=null) {
				MainWindow.Instance.ShowModel(
					model,
					this.identifiable
				);
			}
		}

		protected void handlerView(object sender,EventArgs e)
		{
			MainWindow.Instance.Current=this.identifiable;
		}

		protected void handlerSiblings(object sender,EventArgs e)
		{
			this.ShowSiblings();
		}

		protected void handlerDescendants(object sender,EventArgs e)
		{
			this.ShowDescendants();
		}

		protected void handlerEdit(object sender,EventArgs e)
		{
			if(this.editd==null) {
				this.editd=new Dialogs.Edit(this.identifiable,this);
				this.editd.Show();
			}
		}

		protected void handlerNotes(object sender,EventArgs e)
		{
			if(this.noted==null) {
				this.noted=new Dialogs.Note(this.identifiable,this);
				this.noted.Show();
			}
		}

		public void FinishEdit()
		{
			this.editd=null;
		}

		public void FinishNote()
		{
			this.noted=null;
		}

		protected void handlerCollapse(object sender,EventArgs e)
		{
			this.ToggleChildCollapseState();
		}
	}
}

