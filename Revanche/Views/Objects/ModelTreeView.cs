using System;
using Revanche.Models;
using System.Linq;
using Revanche.Lib;
using System.Text.RegularExpressions;

namespace Revanche.Views.Objects
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class ModelTreeView : Gtk.Bin
	{
		private IdentifiableSet setObj;

		public Models.Identifiable DescendsFrom{
			get{
				return this.setObj.Parent;
			}
		}

		public ModelTreeView()
		{
			this.Build();
		}

		public void Initialize(IdentifiableSet setObj)
		{
			this.setObj=setObj;
			if(!setObj.Extendable) {
				this.addButton.Destroy();
			}
			this.Refresh();
			this.ShowAll();
		}
		
		public void Refresh()
		{
			for(int i=treeview1.Columns.Length-1;i>=0;i--){
				treeview1.RemoveColumn(treeview1.GetColumn(i));
			}

			if(this.setObj.Objects.Count()>0) {
				if(!this.setObj.SameParent) {
					Gtk.TreeViewColumn tvc2=new Gtk.TreeViewColumn();
					tvc2.Title=" Path";
					Gtk.CellRendererText crt2=new Gtk.CellRendererText();
					tvc2.PackStart(crt2,true);
					treeview1.AppendColumn(tvc2);
					tvc2.SetCellDataFunc(crt2,renderField);
				}

				Gtk.TreeViewColumn tvc=new Gtk.TreeViewColumn();
				tvc.Title=" Title";
				Gtk.CellRendererText crt=new Gtk.CellRendererText();
				tvc.PackStart(crt,true);
				treeview1.AppendColumn(tvc);
				tvc.SetCellDataFunc(crt,renderField);

				if(this.setObj.Model==null) {
					Gtk.TreeViewColumn tvc3=new Gtk.TreeViewColumn();
					tvc3.Title=" Type";
					Gtk.CellRendererText crt3=new Gtk.CellRendererText();
					tvc3.PackStart(crt3,true);
					treeview1.AppendColumn(tvc3);
					tvc3.SetCellDataFunc(crt3,renderField);
				}
			}

			foreach(string property in this.setObj.Columns){
				Gtk.TreeViewColumn tvc=new Gtk.TreeViewColumn();
				tvc.Title=(new Regex("(.)([A-Z])")).Replace(property,"$1 $2");
				Gtk.CellRendererText crt=new Gtk.CellRendererText();
				tvc.PackStart(crt,true);
				treeview1.AppendColumn(tvc);
				tvc.SetCellDataFunc(crt,renderField);
			}
			Gtk.ListStore store=new Gtk.ListStore(typeof(Models.Identifiable));
			foreach(Models.Identifiable i in this.setObj.Objects) {
				store.AppendValues(i);
			}
			this.treeview1.Model=store;
		}

		protected void handlerRowActivated(object sender,Gtk.RowActivatedArgs e)
		{
			Gtk.TreeIter iter=new Gtk.TreeIter();
			this.treeview1.Selection.GetSelected(out iter);
			MainWindow.Instance.Current=this.treeview1.Model.GetValue(iter,0) as Models.Identifiable;
		}
		
		protected void handlerAdd(object sender,EventArgs e)
		{
			MainWindow.Instance.CreateObject(this.setObj.Parent,this.setObj.Model);
		}

		private void renderField(Gtk.TreeViewColumn column,Gtk.CellRenderer cell,Gtk.TreeModel treemodel,Gtk.TreeIter iter)
		{
			Models.Identifiable i=treemodel.GetValue(iter,0) as Models.Identifiable;

			object s="";
			if(column.Title==" Title") {
				s=i.Title;
			} else if(column.Title==" Path") {
				string[] pathComponents=i.BuildSpecifierByTitle().Split(':')[2].Substring(1).Split('/');
				s="/"+string.Join("/",pathComponents.Take(pathComponents.Length-1))+"/";
				s=(string)s=="//"?"/":s;
			}else if(column.Title==" Type") {
				s=i.GetModelInfo().Id;
			} else {
				string title=column.Title.Replace(" ","");
				s=i.Property(title);
				if(i.GetTypeInfo(title).Id=="BOOLEAN") {
					s=((s as bool?)??false)?"Yes":"No";
				} else if(i.GetTypeInfo(title).Id=="TIMESTAMP") {
					DateTime dt=new DateTime((s as long?)??0L);
					s=dt.ToString(Config.DATE_FORMAT+" "+Config.TIME_FORMAT);
				} else if((s as string)!=null&&Locator.IsSpecifier(s as string)) {
					Models.Heirarchical h=Locator.Find(s as string,i);
					if(h!=null&&h is Models.Identifiable) {
						s=(h as Models.Identifiable).Title;
					}
				}
			}

			(cell as Gtk.CellRendererText).Text=(s??"").ToString();
		}
	}
}

