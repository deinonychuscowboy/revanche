using System;
using System.Text.RegularExpressions;
using Revanche.Lib;

namespace Revanche.Views.Objects
{
	/// <summary>
	/// UI component which renders an identifiable's properties. Does this as a flat list using Displays.* objects. Also shows notes in tooltips.
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class Properties : Gtk.Bin
	{
		private Models.Identifiable identifiable;

		public Properties()
		{
			this.Build();
		}

		public void Initialize(Models.Identifiable identifiable)
		{
			this.identifiable=identifiable;
		}

		public void Refresh()
		{
			foreach(Gtk.Widget child in this.table1.Children){
				child.Destroy();
			}
			string[] properties=identifiable.GetModelInfo().GetPropertyNames();

			uint propertyLength=0;
			foreach(string key in properties){
				object value=identifiable.Property(key);
				if(identifiable.GetTypeInfo(key).ShowDefault
					||identifiable.GetTypeInfo(key).DefaultValue!=null&&!identifiable.GetTypeInfo(key).DefaultValue.Equals(value)
					||value!=null&&!value.Equals(identifiable.GetTypeInfo(key).DefaultValue)
				){
					propertyLength++;
				}
			}

			this.table1.NRows=propertyLength;
			this.table1.RowSpacing=Config.PROPERTY_PADDING;
			uint counter=0;
			foreach(string key in properties){
				object value=identifiable.Property(key);
				if(identifiable.GetTypeInfo(key).ShowDefault
					||identifiable.GetTypeInfo(key).DefaultValue!=null&&!identifiable.GetTypeInfo(key).DefaultValue.Equals(value)
					||value!=null&&!value.Equals(identifiable.GetTypeInfo(key).DefaultValue)
				){
					Gtk.Label label=new Gtk.Label("<b>"+(new Regex("(.)([A-Z])")).Replace(key,"$1 $2")+"</b>");
					label.SetAlignment(0,0.5f);
					label.UseMarkup=true;
					Gtk.Widget widget=label;
					this.table1.Attach(widget,0,1,counter,counter+1);
					(this.table1[widget] as Gtk.Table.TableChild).XOptions=Gtk.AttachOptions.Fill;
					string propertyNote=identifiable.PropertyNote(key);
					if(identifiable.GetTypeInfo(key).Id=="LINK"||identifiable.GetTypeInfo(key).Id.StartsWith("RELATION")){
						widget=new Displays.Button(value as string,propertyNote!="");
					} else if(identifiable.GetTypeInfo(key).Id=="COLOR"){
						widget=new Displays.Color(value as string,propertyNote!="");
					} else{
						widget=new Displays.Text(value??"",propertyNote!="");
					}
					widget.TooltipText=propertyNote;
					this.table1.Attach(widget,1,2,counter,counter+1);
					(this.table1[widget] as Gtk.Table.TableChild).XOptions=Gtk.AttachOptions.Expand|Gtk.AttachOptions.Fill;
					counter++;
				}
			}
			this.ShowAll();
			this.RefreshLayout();
		}

		public void RefreshLayout(){
		}
	}
}

