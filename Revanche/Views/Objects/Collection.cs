using System;
using System.Linq;
using System.Collections.Generic;
using Revanche.Lib;

namespace Revanche.Views.Objects
{
	/// <summary>
	/// UI component which renders an identifiable's collections. Creates a TreeView for each one, listing the contents of the collection in the tree with a column for each of the RevModel's properties.
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class Collection : Gtk.Bin
	{
		private Dictionary<string,ModelTreeView> treeviews;

		public Collection()
		{
			this.Build();
		}

		/// <summary>
		/// For display of normal collections
		/// </summary>
		/// <param name="identifiable">Model.</param>
		public void Initialize(Models.Identifiable identifiable)
		{
			this.treeviews=new Dictionary<string,ModelTreeView>();
			foreach(string key in identifiable.GetModelInfo().Collection) {
				treeviews[key]=new ModelTreeView();
				treeviews[key].Initialize(new IdentifiableSet(identifiable,key));

				Gtk.Label label=new Gtk.Label();
				label.UseMarkup=true;
				label.LabelProp="<b>"+key+"s</b>";
				label.SetAlignment(0,0.5f);

				Gtk.VBox containerBox=new Gtk.VBox();
				containerBox.Spacing=0;

				containerBox.PackStart(label,false,false,0);

				containerBox.PackStart(treeviews[key],true,true,0);

				this.vbox.PackStart(containerBox,true,true,0);
			}
			this.Refresh();
			this.ShowAll();
		}

		/// <summary>
		/// For display of null parents, called by the home screen.
		/// </summary>
		/// <param name="models">Models.</param>
		public void Initialize(Models.Identifiable[] objects)
		{
			this.treeviews=new Dictionary<string,ModelTreeView>();
			treeviews[" HOME"]=new ModelTreeView();
			treeviews[" HOME"].Initialize(new IdentifiableSet(objects));

			Gtk.VBox containerBox=new Gtk.VBox();
			containerBox.Spacing=0;

			containerBox.PackStart(treeviews[" HOME"],true,true,0);

			this.vbox.PackStart(containerBox,true,true,0);

			this.Refresh();
			this.ShowAll();
		}

		public void RefreshLayout(){}

		public void Refresh()
		{
			foreach(ModelTreeView mtv in this.treeviews.Values) {
				mtv.Refresh();
			}
		}
	}
}

