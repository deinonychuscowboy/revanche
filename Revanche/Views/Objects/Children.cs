using System;
using System.Linq;

namespace Revanche.Views.Objects
{
	/// <summary>
	/// UI component which renders an identifiable's Children. Creates a new Objects.Identifiable for each one, lists them in a Vbox.
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class Children : Gtk.Bin
	{
		private Models.Identifiable identifiable;
		private Views.Objects.Identifiable parent;

		public bool Collapsed{
			get{
				return this.vbox.Children.Select(x => (x as Identifiable).Collapsed).Aggregate(true,(current,item) => current&&item);
			}
		}

		public Children()
		{
			this.Build();
		}

		public void Initialize(Models.Identifiable identifiable,Views.Objects.Identifiable parent)
		{
			this.identifiable=identifiable;
			this.parent=parent;
			string[] children=identifiable.GetModelInfo().Children;
			foreach(string key in children){
				Identifiable child=new Identifiable(identifiable.GetChild(key),parent);
				this.vbox.PackStart(child);
				child.Show();
			}
			this.Show();
			this.RefreshLayout();
		}

		public void Refresh()
		{
			foreach(Gtk.Widget child in this.vbox.Children){
				(child as Identifiable).Refresh();
			}
		}

		public void RefreshLayout(){
			foreach(Gtk.Widget child in this.vbox.Children){
				(child as Identifiable).RefreshLayout();
			}
		}

		public void CollapseAll(){
			foreach(Gtk.Widget child in this.vbox.Children){
				(child as Identifiable).CollapseAll();
			}
		}

		public void ExpandAll(){
			foreach(Gtk.Widget child in this.vbox.Children){
				(child as Identifiable).ExpandAll();
			}
		}
	}
}

