using System;

namespace Revanche.Views.Dialogs
{
	/// <summary>
	/// Dialog which confirms deletion of an object (for Collection objects, or refuses it for Children)
	/// </summary>
	public partial class Delete : Gtk.Dialog
	{
		private Models.Identifiable identifiable;
		private bool canDelete;

		public Delete(Models.Identifiable identifiable)
		{
			this.identifiable=identifiable;
			this.Build();
			this.Title="Delete "+identifiable.Title;
			if(this.identifiable.Parent==null||Array.IndexOf(this.identifiable.Parent.GetCollection(identifiable.GetModelInfo().Id),identifiable)!=-1){
				canDelete=true;
				this.label1.LabelProp="Are you sure you want to delete the "+identifiable.GetModelInfo().Id+" '"+identifiable.Title+"'?";
			} else{
				canDelete=false;
				this.label1.LabelProp="The node '"+identifiable.Title+"' is not part of a group, and cannot be deleted without deleting its parent.";
				this.buttonOk.Hide();
			}
		}

		protected void handlerOkay(object sender,EventArgs e)
		{
			if(canDelete){
				Controller.Dirty=true;
				identifiable.Delete();
				MainWindow.Instance.Current=identifiable.Parent;
			}
			this.Destroy();
		}

		protected void handlerCancel(object sender,EventArgs e)
		{
			this.Destroy();
		}
	}
}

