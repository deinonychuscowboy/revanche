using System;
using System.Linq;
using Revanche.Lib;
using Gtk;

namespace Revanche.Views.Dialogs
{
	/// <summary>
	/// Dialog which displays any exceptions that may occur, as well as the first-time welcome message (NoModelsException)
	/// </summary>
	public partial class Error : Gtk.Dialog
	{
		public Error(Exception ex,bool fatal=true)
		{
			this.Build();
			this.dialogImage.WidthRequest = 128;
			this.dialogImage.HeightRequest = 128;
			this.dialogImage.IconSize=(int)IconSize.Dialog;
			this.exampleButtons.Hide();
			if(ex.GetType()==typeof(NoModelsException)){
				this.Title="Welcome";
				this.dialogImage.Stock=Lib.IconSelector.DialogInfo;
				ErrorPipe.Unstack();
				this.label2.LabelProp="Welcome to Revanche!\n\nIt looks like this is your first time running the '"+Revanche.Lib.Profile.Current+"' profile.\nNo models were found in '"+Revanche.Lib.Profile.StructFolder+"'.\n\nRevanche cannot run until models are defined.\nI have created a STRUCTURE.md file in this folder to help you set up this profile. If this is your first time running Revanche, you may also want to create some of the example projects below.\n\nIf this is not your first time running Revanche, please make sure your configuration in '"+Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+System.IO.Path.DirectorySeparatorChar+".revanche"+"' is pointing to the correct directory.";
				this.buttonCancel.Hide();
				this.exampleButtons.Show();
			} else if(ex.GetType()==typeof(RemovedStructuralElementException)){
				this.WidthRequest=800;
				this.label2.WidthRequest=650;
				this.dialogImage.Stock=Lib.IconSelector.DialogWarning;
				ErrorPipe.Unstack();
				this.label2.LabelProp="Revanche encountered some errors loading your data. No data has been lost.\nRevanche will now abort loading to give you a chance to fix the problem.\n\nThis is likely due to a recent change to the files in '"+Revanche.Lib.Profile.StructFolder+"'.\nIf any fields were removed or renamed, you will need to correct the files in '"+Revanche.Lib.Profile.DataFolder+"'.\n\nThe fields marked '!!!' below need to be corrected.\nIf you renamed any fields, the new name should be listed as well to help you.\n\n"+ex.Message;
				this.buttonCancel.Hide();
			} else if(!fatal){
				this.dialogImage.Stock=Lib.IconSelector.DialogWarning;
				this.label2.LabelProp=ex.Message;
				Exception[] exceptions=ErrorPipe.Unstack();
				if(exceptions.Length>0){
					this.label2.LabelProp+="\n\n"+String.Join("\n",from Exception e in exceptions select e.Message);
				}
				this.buttonCancel.Hide();
			} else{
				this.dialogImage.Stock=Lib.IconSelector.DialogError;
				this.label2.LabelProp="(Stage "+Controller.Stage+") An error was encountered, and Revanche must close.\n\n"+ex.Message;
				Exception[] exceptions=ErrorPipe.Unstack();
				if(exceptions.Length>0){
					this.label2.LabelProp+="\n\n"+String.Join("\n",from Exception e in exceptions select e.Message);
				}
				this.buttonOk.Hide();
			}
		}
		public Error(string msg)
		{
			this.Build();
			this.dialogImage.SetFromStock(Lib.IconSelector.DialogWarning,IconSize.Dialog);
			this.label2.LabelProp=msg;
			this.buttonCancel.Hide();
		}

		protected void handleQuitButton(object sender,EventArgs e)
		{
			this.Destroy();
			Controller.Reset();
		}

		protected void handleOkayButton(object sender,EventArgs e)
		{
			this.Destroy();
		}
	}
}

