﻿using System;
using System.Text.RegularExpressions;
using Gtk;

namespace Revanche.Views.Dialogs
{
	public partial class Profile : Gtk.Dialog
	{
		private string action;
		private string oldName;
		private TreeIter iter;
		private Regex test;

		public static Profile Instance { get; private set; }

		public Profile(bool running)
		{
			if(Profile.Instance!=null) {
				throw new InvalidOperationException();
			}
			Profile.Instance=this;

			this.test=new Regex("^[a-zA-Z0-9-]*$");
			this.Build();
			this.ActionArea.Destroy(); // destroy built-in dialog action area so we can control our own actions but still be treated as a dialog

			this.treeview1.CanFocus=true;

			Gtk.TreeViewColumn tvc=new Gtk.TreeViewColumn();
			tvc.Title="Name";
			Gtk.CellRendererText crt=new Gtk.CellRendererText();
			tvc.PackStart(crt,true);
			this.treeview1.AppendColumn(tvc);
			tvc.SetCellDataFunc(crt,renderField);

			this.RefreshList();

			this.treeview1.Selection.Mode=SelectionMode.Single;

			this.Modal=true;
			this.ShowAll();
			this.actions.Show();
			this.editor.Hide();
			this.exampleButtons.Hide();

			if(running) {
				this.SetCancel();
			} else {
				this.SetQuit();
			}
		}

		public void RefreshList(){
			Gtk.ListStore store=new Gtk.ListStore(typeof(string));
			foreach(string profile in Revanche.Lib.Profile.Profiles) {
				store.AppendValues(profile);
			}
			this.treeview1.Model=store;
		}

		private void renderField(Gtk.TreeViewColumn column,Gtk.CellRenderer cell,Gtk.TreeModel treemodel,Gtk.TreeIter iter)
		{
			(cell as Gtk.CellRendererText).Text=(string)treemodel.GetValue(iter,0);
		}

		public override void Destroy(){
			Profile.Instance=null;
			base.Destroy();
		}

		public void SetCancel(){
			buttonQuit.Label="Cancel";
			buttonQuit.Image=new Image();
			(buttonQuit.Image as Image).SetFromStock(Lib.IconSelector.Cancel,IconSize.Button);
		}

		public void SetQuit(){
			buttonQuit.Label="Quit";
			buttonQuit.Image=new Image();
			(buttonQuit.Image as Image).SetFromStock(Lib.IconSelector.Quit,IconSize.Button);
		}

		public void Quit(){
			this.handleQuit(null,EventArgs.Empty);
		}

		protected void handleDeleteEvent(object sender, DeleteEventArgs e){
			this.handleQuit(sender,e);
		}

		protected void handleQuit (object sender, EventArgs e)
		{
			if(buttonQuit.Label=="Quit") {
				Controller.RequestTermination();
			}
			this.Destroy();
		}

		protected void handleOpen (object sender, EventArgs e)
		{
			if(this.treeview1.Selection.CountSelectedRows()>0){
				TreeIter iter;
				this.treeview1.Selection.GetSelected(out iter);

				string name=(string)this.treeview1.Model.GetValue(iter,0);

				Controller.SelectProfile(name);
			}
			this.Destroy();
		}

		protected void handleAdd (object sender, EventArgs e)
		{
			this.actions.Hide();
			this.editor.Show();
			this.entry2.GrabFocus();
			this.action="add";
		}

		protected void handleExamples (object sender, EventArgs e)
		{
			this.showExamples.Hide();
			this.exampleButtons.Show();
		}

		protected void handleExampleCreated (object sender, EventArgs e)
		{
			this.RefreshList();
			this.showExamples.Show();
			this.exampleButtons.Hide();
		}

		protected void handleDelete (object sender, EventArgs e)
		{
			if(this.treeview1.Selection.CountSelectedRows()>0){
				TreeIter iter;
				this.treeview1.Selection.GetSelected(out iter);

				string name=(string)this.treeview1.Model.GetValue(iter,0);

				if(Controller.DeleteProfile(name)){
					(this.treeview1.Model as ListStore).Remove(ref iter);
				}

			}
		}

		protected void handleEdit (object sender, EventArgs e)
		{
			if(this.treeview1.Selection.CountSelectedRows()>0){
				TreeIter iter;
				this.treeview1.Selection.GetSelected(out iter);

				string name=(string)this.treeview1.Model.GetValue(iter,0);

				this.oldName=name;
				this.iter=iter;

				this.action="edit";
				this.actions.Hide();
				this.editor.Show();
				this.entry2.GrabFocus();
			}
		}

		protected void handleEditApprove (object sender, EventArgs e)
		{
			string newname=this.entry2.Text;

			if(newname=="") {
				this.editor.Hide();
				this.actions.Show();
				this.action=null;
			}else if(this.test.IsMatch(newname)){
				if(this.action=="add"){
					if(Controller.CreateProfile(newname)){
						(this.treeview1.Model as ListStore).AppendValues(newname);
					}
				} else if(this.action=="edit"){
					if(Controller.MoveProfile(this.oldName,newname)){
						(this.treeview1.Model as ListStore).Remove(ref this.iter);
						(this.treeview1.Model as ListStore).AppendValues(newname);
					}
				}

				this.editor.Hide();
				this.actions.Show();
				this.action=null;
			}else{
				Error errmsg=new Error("The name contains an invalid character. Valid characters are letters, numbers, and hyphens.");
				errmsg.Run();
			}
		}

		protected void handlerRowActivated(object sender,Gtk.RowActivatedArgs e)
		{
			Gtk.TreeIter iter=new Gtk.TreeIter();
			treeview1.Selection.GetSelected(out iter);

			string name=(string)this.treeview1.Model.GetValue(iter,0);

			Controller.SelectProfile(name);
			this.Destroy();
		}

		protected void handleEditKey(object sender, Gtk.KeyPressEventArgs e){
			// TODO Esc does not properly exit the edit box here because this is a dialog (instead it closes the dialog)
			// This should really be a window, but that's a big change I don't want to make right now
			if(e.Event.Key==Gdk.Key.Escape||e.Event.Key==Gdk.Key.Return) {
				if(e.Event.Key==Gdk.Key.Escape) {
					this.entry2.Text="";
				}
				this.handleEditApprove(sender,e);
			}
		}
	}
}

