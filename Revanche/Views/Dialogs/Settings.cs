using System;
using System.Linq;
using Revanche.Lib;

namespace Revanche.Views.Dialogs
{
	/// <summary>
	/// Dialog which displays Revanche settings (i.e. edits the Config object)
	/// </summary>
	public partial class Settings : Gtk.Dialog
	{
		public Settings()
		{
			this.Build();
			this.combobox1.Active=!Config.BUTTON_ICONS?1:!Config.BUTTON_TEXT?0:2;
			this.entry1.Text=Config.ROOT;
			switch(Config.NOTE_STYLE){
				case "i":
					this.combobox2.Active=0;
					break;
				case "u":
					this.combobox2.Active=1;
					break;
				case "red":
					this.combobox2.Active=2;
					break;
				case "yellow":
					this.combobox2.Active=3;
					break;
				case "green":
					this.combobox2.Active=4;
					break;
				case "blue":
					this.combobox2.Active=5;
					break;
			}
			this.spinbutton1.Value=Config.PROPERTY_PADDING;
			this.entry2.Text=Config.DATE_FORMAT;
			this.entry3.Text=Config.TIME_FORMAT;
			this.togglebutton1.Active=Config.COLLAPSE_HIDE_BUTTONS;
			this.togglebutton1.Label=Config.COLLAPSE_HIDE_BUTTONS?"ON":"OFF";
			this.togglebutton2.Active=Config.COLLAPSE_START;
			this.togglebutton2.Label=Config.COLLAPSE_START?"ON":"OFF";
			this.nonstupidcombobox1.SetContents((new string[]{"< Ask Every Time >"}).Union(Revanche.Lib.Profile.Profiles).ToArray(),typeof(string));
			this.nonstupidcombobox1.SetActive("< Ask Every Time >");
			this.nonstupidcombobox1.SetActive(Revanche.Lib.Profile.HasDefault?Revanche.Lib.Profile.Default:"< Ask Every Time >");
		}

		protected void handlerCancel(object sender,EventArgs e)
		{
			this.Destroy();
		}

		protected void handlerOkay(object sender,EventArgs e)
		{
			switch(this.combobox1.Active){
				case 0:
					Config.BUTTON_ICONS=true;
					Config.BUTTON_TEXT=false;
					break;
				case 1:
					Config.BUTTON_ICONS=false;
					Config.BUTTON_TEXT=true;
					break;
				case 2:
					Config.BUTTON_ICONS=true;
					Config.BUTTON_TEXT=true;
					break;
			}
			Config.ROOT=this.entry1.Text;
			switch(this.combobox2.Active){
				case 0:
					Config.NOTE_STYLE="i";
					break;
				case 1:
					Config.NOTE_STYLE="u";
					break;
				case 2:
					Config.NOTE_STYLE="red";
					break;
				case 3:
					Config.NOTE_STYLE="yellow";
					break;
				case 4:
					Config.NOTE_STYLE="green";
					break;
				case 5:
					Config.NOTE_STYLE="blue";
					break;
			}
			Config.PROPERTY_PADDING=(uint)this.spinbutton1.ValueAsInt;
			Config.DATE_FORMAT=this.entry2.Text;
			Config.TIME_FORMAT=this.entry3.Text;
			Config.COLLAPSE_HIDE_BUTTONS=this.togglebutton1.Active;
			Config.COLLAPSE_START=this.togglebutton2.Active;
			Config.DEFAULT_PROFILE=(string)this.nonstupidcombobox1.GetActive()=="< Ask Every Time >"?"":(string)this.nonstupidcombobox1.GetActive();
			Config.Save();
			this.Destroy();
		}

		protected void handlerUpdateLabel1 (object sender, EventArgs e)
		{
			this.togglebutton1.Label=this.togglebutton1.Active?"ON":"OFF";
		}

		protected void handlerUpdateLabel2 (object sender, EventArgs e)
		{
			this.togglebutton2.Label=this.togglebutton2.Active?"ON":"OFF";
		}
	}
}

