using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

namespace Revanche.Views.Dialogs
{
	/// <summary>
	/// Dialog which edits an Identifiable's properties and displays all Fields.* widgets
	/// </summary>
	public partial class Edit : Gtk.Dialog
	{
		private bool handlingTriggers;
		private Models.Identifiable identifiable;
		private Objects.Identifiable parent;
		private Dictionary<string,ValuedField> fields;

		public Edit(Models.Identifiable identifiable,Objects.Identifiable parent)
		{
			this.identifiable=identifiable;
			this.parent=parent;
			this.fields=new Dictionary<string, ValuedField>();
			this.Build();
			this.Title="Edit "+parent.Title;
			this.idlabel.Text=identifiable.Id;
			string[] properties=identifiable.GetModelInfo().GetPropertyNames();

			bool reparentable=Array.IndexOf(Revanche.Models.RevModel.Collectables,identifiable.GetModelInfo())>=0;

			bool hasHelpNote=identifiable.HelpNote()!="";

			this.table1.NRows=(uint)(properties.Length+(hasHelpNote?1:0)+(reparentable?1:0));
			uint counter=0;

			string lastKey=null;

			if(hasHelpNote){
				Gtk.Label helpNotes=new Gtk.Label(identifiable.HelpNote());
				helpNotes.Wrap=true;
				this.table1.Attach(helpNotes,0,2,counter,counter+1);
				(this.table1[helpNotes] as Gtk.Table.TableChild).XOptions=Gtk.AttachOptions.Fill;
				counter++;
			}

			if(reparentable){
				// This code is largely similar 
				Gtk.Label label=new Gtk.Label("<b>Parent</b>");
				label.SetAlignment(0,0.5f);
				label.UseMarkup=true;
				label.TooltipText="This object is in a collection, and can be reassigned to a different parent object.";
				this.table1.Attach(label,0,1,counter,counter+1);
				(this.table1[label] as Gtk.Table.TableChild).XOptions=Gtk.AttachOptions.Fill;

				List<string> options=new List<string>();
				options.Add(null);
				foreach(Models.RevModel rev in identifiable.GetModelInfo().GetParents()){
					options.AddRange(from Models.Identifiable i in rev.GetImplementations() select i.BuildSpecifierById());
				}

				ValuedField widget=new Fields.Select(options.ToArray(),identifiable.Parent==null?null:identifiable.Parent.BuildSpecifierById(),typeof(string),true);
				this.table1.Attach(widget as Gtk.Widget,1,2,counter,counter+1);
				(this.table1[widget as Gtk.Widget] as Gtk.Table.TableChild).XOptions=Gtk.AttachOptions.Expand|Gtk.AttachOptions.Fill;
				fields.Add("_ Parent _",widget);
				lastKey="_ Parent _";
				counter++;
			}
			foreach(string key in properties){
				Gtk.Label label=new Gtk.Label("<b>"+(new Regex("(.)([A-Z])")).Replace(key,"$1 $2")+"</b>");
				label.SetAlignment(0,0.5f);
				label.UseMarkup=true;

				List<string> propertyHelpNoteList=new List<string>();
				foreach(string note in identifiable.PropertyHelpNote(key)){
					if(note!=""){
						propertyHelpNoteList.Add(note);
					}
				}
				string propertyHelpNote=String.Join("\n",propertyHelpNoteList);
				if(propertyHelpNote!=""){
					label.TooltipText=propertyHelpNote;
				}

				this.table1.Attach(label,0,1,counter,counter+1);
				(this.table1[label] as Gtk.Table.TableChild).XOptions=Gtk.AttachOptions.Fill;
				ValuedField widget;
				Types.RevType type=identifiable.GetTypeInfo(key);
				if(type.Id=="COLOR") {
					widget=new Fields.Color(identifiable.Property(key) as string);
				} else if(type.Id=="BOOLEAN") {
					widget=new Fields.Toggle((identifiable.Property(key) as bool?)??false);
				} else if(type.Id=="TRIGGER") {
					widget=new Fields.Apply();
				}else if(type.Id.StartsWith("RELATION")){
					List<Models.Identifiable> options=new List<Models.Identifiable>();
					options.Add(null);
					string[] models=type.Id.Split(' ').Skip(1).ToArray();
					if(models.Length==0) {
						options.AddRange(Models.Identifiable.GetRegisteredIdentifiables());
					}else{
						foreach(string m in models){
							options.AddRange(Models.RevModel.GetRegisteredModel(m).GetImplementations());
						}
					}
					widget=new Fields.Select((from Models.Identifiable i in options select i==null?null:i.BuildSpecifierById()).ToArray(),identifiable.Property(key),typeof(string),models.Length!=1);
				} else if(type.ContentType==Revanche.Types.Content.ENUMERATED){
					widget=new Fields.Select(type.Values,(identifiable.Property(key)??"").ToString(),typeof(string));
				} else if(type.ContentType==Revanche.Types.Content.NULLABLE_ENUMERATED){
					widget=new Fields.Select((new object[]{null}).Concat(type.Values).ToArray(),identifiable.Property(key)?.ToString(),typeof(string));
				} else if(type.Id=="DECIMAL"){
					widget=new Fields.Decimal((identifiable.Property(key) as double?)??0.0);
				} else if(type.Id=="INTEGER"){
					widget=new Fields.Integer((identifiable.Property(key) as int?)??0);
				} else if(type.Id=="TIMESTAMP"){
					widget=new Fields.Date((identifiable.Property(key) as long?)??0L);
				} else{
					// safest if we don't know the right one
					widget=new Fields.Text((identifiable.Property(key)??"").ToString());
				}

				List<string> valuesHelpNoteList=new List<string>();
				foreach(string note in type.ValuesHelpNote()){
					if(note!=""){
						valuesHelpNoteList.Add(note);
					}
				}
				string valuesHelpNote=String.Join("\n",valuesHelpNoteList);
				if(valuesHelpNote!=""){
					(widget as Gtk.Widget).TooltipText=valuesHelpNote;
				}
				string lastKeyLocal=lastKey;
				if(lastKeyLocal!=null){
					this.fields[lastKeyLocal].Tabbed+=(object sender,EventArgs e) =>{
						widget.Focus();
					};
					widget.BackTabbed+=(object sender,EventArgs e) =>{
						this.fields[lastKeyLocal].Focus();
					};
				}
				this.table1.Attach(widget as Gtk.Widget,1,2,counter,counter+1);
				(this.table1[widget as Gtk.Widget] as Gtk.Table.TableChild).XOptions=Gtk.AttachOptions.Expand|Gtk.AttachOptions.Fill;
				this.fields[key]=widget;
				lastKey=key;
				widget.Changed+=this.handlerPropertyChange;
				counter++;
			}
			this.ShowAll();
		}

		public void Refresh(Dictionary<string,object> data=null)
		{
			if(data!=null){
				foreach(KeyValuePair<string,ValuedField> field in this.fields){
					field.Value.Value=data[field.Key];
				}
			}
		}

		protected void handlerCancel(object sender,EventArgs e)
		{
			this.parent.FinishEdit();
			this.Destroy();
		}

		protected void handlerOkay(object sender,EventArgs e)
		{
			Controller.Dirty=true;
			string[] properties=this.identifiable.GetModelInfo().GetPropertyNames();
			uint counter=0;
			foreach(string key in properties){
				identifiable.SetProperty(key,this.fields[key].Value);
				counter++;
			}

			if(this.fields.ContainsKey("_ Parent _")&&(this.identifiable.Parent==null||this.identifiable.Parent.Id!=this.fields["_ Parent _"].Value as string)){
				string parentVal=this.fields["_ Parent _"].Value as string;
				this.identifiable.Reparent((parentVal==null?null:Locator.Find(parentVal)) as Models.Identifiable);
			}

			MainWindow.Instance.RefreshView();
			this.parent.FinishEdit();
			this.Destroy();
		}

		protected void handlerPropertyChange(object sender,EventArgs e)
		{
			if(!this.handlingTriggers){
				Dictionary<string,object> result=null;
				foreach(KeyValuePair<string,ValuedField> field in this.fields){
					if(sender==field.Value){
						this.handlingTriggers=true;
						Dictionary<string,object> currentData=new Dictionary<string,object>();
						foreach(KeyValuePair<string,ValuedField> dataField in this.fields){
							currentData[dataField.Key]=dataField.Value.Value;
						}
						result=identifiable.CalculateTriggers(field.Key,field.Value.Value,currentData);
						break;
					}
				}
				this.Refresh(result);
				this.handlingTriggers=false;
			}
		}
	}
}

