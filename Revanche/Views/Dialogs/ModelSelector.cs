using System;
using System.Linq;

namespace Revanche.Views.Dialogs
{
	/// <summary>
	/// Dialog which displays models which may be selected.
	/// </summary>
	public partial class ModelSelector : Gtk.Dialog
	{
		private bool createMode;
		public Models.RevModel Selected{ get; private set; }

		public ModelSelector(Models.RevModel[] models,bool createMode=true)
		{
			this.Build();
			string[] specifiers=models.Select(model => model.BuildSpecifier()).ToArray();
			this.nonstupidcombobox1.SetContents(specifiers,typeof(string));
			if(specifiers.Length>0){
				this.nonstupidcombobox1.SetActive(specifiers[0]);
			}
			this.createMode=createMode;
		}

		protected void handlerOkay(object sender,EventArgs e)
		{
			this.Selected=Locator.Find(this.nonstupidcombobox1.GetActive() as string) as Models.RevModel;
			this.Destroy();
		}

		protected void handlerCancel(object sender,EventArgs e)
		{
			this.Destroy();
		}
	}
}

