using System;

namespace Revanche
{
	/// <summary>
	/// Interface for fields that edit values.
	/// </summary>
	public interface ValuedField
	{
		/// <summary>
		/// Gets the current value from the UI.
		/// </summary>
		/// <value>The value.</value>
		object Value{ get; set; }

		/// <summary>
		/// Occurs when the user presses Tab
		/// </summary>
		event EventHandler Tabbed;
		/// <summary>
		/// Occurs when the user presses Shift + Tab
		/// </summary>
		event EventHandler BackTabbed;
		/// <summary>
		/// Occurs when changed.
		/// </summary>
		event EventHandler Changed;

		/// <summary>
		/// Grabs focus into the editable component of this field.
		/// </summary>
		void Focus();
	}
}

