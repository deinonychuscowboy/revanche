using System;
using System.Text.RegularExpressions;

namespace Revanche.Views.Fields
{
	/// <summary>
	/// Edit field for TEXT type. Starts as a single line, automagically grows vertically as the user types, and then conjures a scrollbar if it gets too tall.
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class Text : Gtk.Bin, ValuedField
	{
		private static Regex tabReplace=new Regex(@"\t",RegexOptions.Compiled);
		private static Regex firstcharReplace=new Regex(@"\n\s*",RegexOptions.Compiled);
		private Gtk.ScrolledWindow sw;
		private bool usingSW;

		public object Value
		{
			get {
				return Text.firstcharReplace.Replace(Text.tabReplace.Replace(this.textview1.Buffer.Text,"    "),"\n");
			}
			set {
				this.textview1.Buffer.Text=(string)value;
			}
		}

		public event EventHandler Tabbed;
		public event EventHandler BackTabbed;
		public event EventHandler Changed;

		public Text(string text)
		{
			this.usingSW=false;
			this.sw=new Gtk.ScrolledWindow();
			sw.HeightRequest=150;
			this.Build();
			this.textview1.Buffer.Text=text;
			handleSizeChange(null,EventArgs.Empty);
			this.textview1.Buffer.Changed+=handleSizeChange;
			this.textview1.Buffer.Changed+=handlerChanged;
		}

		protected void handleSizeChange(object sender,EventArgs e)
		{
			if(
				(textview1.Requisition.Height>150
				||textview1.Requisition.Height==0
				&&this.textview1.Buffer.Text.Length>400)
				&&!usingSW) {
				this.alignment1.Remove(textview1);
				this.alignment1.Add(sw);
				sw.Add(textview1);
				usingSW=true;
			} else if(
				(textview1.Requisition.Height<=150
				||textview1.Requisition.Height==0
				&&this.textview1.Buffer.Text.Length<=400)
				&&usingSW) {
				sw.Remove(textview1);
				this.alignment1.Remove(sw);
				this.alignment1.Add(textview1);
				usingSW=false;
			}
			this.ShowAll();
			this.textview1.GrabFocus();
		}

		protected void handlerKey(object o,Gtk.KeyPressEventArgs args)
		{
			if(args.Event.Key==Gdk.Key.Tab) {
				if((args.Event.State&Gdk.ModifierType.ShiftMask)>0) {
					if(BackTabbed!=null) {
						BackTabbed(this,EventArgs.Empty);
					}
					if(Tabbed!=null) {
						Tabbed(this,EventArgs.Empty);
					}
				}
			}
		}

		protected void handlerChanged(object o,EventArgs e)
		{
			if(this.Changed!=null) {
				Changed(this,EventArgs.Empty);
			}
		}

		public void Focus()
		{
			this.textview1.GrabFocus();
		}
	}
}

