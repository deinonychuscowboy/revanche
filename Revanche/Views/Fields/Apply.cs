﻿using System;

namespace Revanche.Views.Fields
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class Apply : Gtk.Bin, ValuedField
	{

		public event EventHandler Tabbed;

		public event EventHandler BackTabbed;

		public event EventHandler Changed;

		public object Value
		{
			get{
				return "TRIGGER";
			}
			set{
			}
		}

		public Apply()
		{
			this.Build();
		}

		public void Focus()
		{
			this.button17.GrabFocus();
		}

		protected void handlerRun(object sender,EventArgs e)
		{
			this.Changed(this,EventArgs.Empty);
		}
	}
}

