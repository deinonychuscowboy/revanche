using System;
using Revanche.Views.Base;

namespace Revanche.Views.Fields
{
	/// <summary>
	/// Edit field for all types of Content.ENUMERATED and Content.NULLABLE_ENUMERATED. ComboBox which can hold either strings (normal) or objects (reparenting)
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class Select : Gtk.Bin, ValuedField
	{
		public object Value
		{
			get{
				return this.nonstupidcombobox1.GetActive();
			}
			set{
				this.nonstupidcombobox1.SetActive(value);
			}
		}

		public event EventHandler Tabbed;
		public event EventHandler BackTabbed;
		public event EventHandler Changed;

		public Select(object[] values,object value,Type type,bool showModels=false)
		{
			this.Build();
			this.nonstupidcombobox1.SetContents(values,type);
			this.nonstupidcombobox1.SetActive(value);
			this.nonstupidcombobox1.Changed+=this.handlerChanged;
			this.nonstupidcombobox1.ShowModels=showModels;
		}

		protected void handlerKey(object o,Gtk.KeyPressEventArgs args)
		{
			if(args.Event.Key==Gdk.Key.Tab){
				if((args.Event.State&Gdk.ModifierType.ShiftMask)>0){
					if(BackTabbed!=null){
						BackTabbed(this,EventArgs.Empty);
					}
					if(Tabbed!=null){
						Tabbed(this,EventArgs.Empty);
					}
				}
			}
		}

		protected void handlerChanged(object o, EventArgs e){
			if(this.Changed!=null){
				Changed(this,EventArgs.Empty);
			}
		}

		public void Focus()
		{
			this.nonstupidcombobox1.GrabFocus();
		}
	}
}

