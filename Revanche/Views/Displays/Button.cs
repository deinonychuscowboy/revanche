﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Revanche.Lib;

namespace Revanche.Views.Displays
{
	public enum ButtonMode{
		INTERNAL,
		EXTERNAL
	}

	[System.ComponentModel.ToolboxItem(true)]
	public partial class Button : Gtk.Bin
	{
		private string data;
		private ButtonMode mode;
		private static Regex specifier=new Regex(@"^(?:ID|TITLE)?:/?[^:/]+(?:/[^:/]+)*/?$");
		private static Regex url=new Regex(@"^https?://(?:[^/\.]+\.)*[^/\.]+(?:/[^/]+)*/?.*$");
		public Button(string dataref,bool note)
		{
			this.Build();
			string data=dataref??"";
			bool isSpec=Locator.IsSpecifier(data);
			bool isUrl=Button.url.IsMatch(data);
			if(data!=""&&!(isSpec||isUrl)) {
				MainWindow.Instance.ShowNonFatalError(new Exception("Malformed "+(this.mode==ButtonMode.INTERNAL?"specifier":"url")+": "+data));
				this.data="";
			} else {
				this.data=data;
				this.mode=isSpec?ButtonMode.INTERNAL:ButtonMode.EXTERNAL;
			}

			if(this.data=="") {
				this.button.Sensitive=false;
			}

			string text="Go";

			if(isSpec&&data!="") {
				Models.Identifiable i=Locator.Find(data) as Models.Identifiable;
				if(i!=null) {
					text=i.Title;
				}
			}

			if(note){
				if(Config.NOTE_STYLE=="i"||Config.NOTE_STYLE=="u"){
					text="<"+Config.NOTE_STYLE+">"+text+"</"+Config.NOTE_STYLE+">";
				}else{
					Gdk.Color textColor=Gdk.Color.Zero;
					Gdk.Color.Parse(Config.NOTE_STYLE,ref textColor);
					this.button.ModifyFg(Gtk.StateType.Normal,textColor);
				}
			}

			this.button.Label=text;
		}

		protected void handleVisit (object sender, EventArgs e)
		{
			if(this.data!="") {
				if(this.mode==ButtonMode.INTERNAL) {
					Models.Heirarchical h=Locator.Find(this.data,MainWindow.Instance.Current);
					if(h==null){
						MainWindow.Instance.ShowNonFatalError(new Exception("No object found for '"+this.data+"'"));
					}
					if(h is Models.Identifiable) {
						MainWindow.Instance.Current=h as Models.Identifiable;
					}
				} else if(this.mode==ButtonMode.EXTERNAL) {
					if(Environment.OSVersion.Platform==PlatformID.Unix||Environment.OSVersion.Platform==PlatformID.MacOSX) {
						Process process=new Process();
						ProcessStartInfo info=new ProcessStartInfo();
						info.WindowStyle=ProcessWindowStyle.Hidden;
						info.FileName="which";
						info.Arguments="exo-open xdg-open kde-open gnome-open gvfs-open";
						info.UseShellExecute=false;
						info.RedirectStandardOutput=true;
						process.StartInfo=info;
						process.Start();
						string first=process.StandardOutput.ReadLine();
						process.WaitForExit();
						process=new Process();
						info=new ProcessStartInfo();
						info.WindowStyle=ProcessWindowStyle.Hidden;
						info.FileName=first;
						info.Arguments=this.data;
						process.StartInfo=info;
						process.Start();
					} else if(Environment.OSVersion.Platform==PlatformID.Win32NT) {
						System.Diagnostics.Process.Start(this.data);
					}
				}
			}
		}
	}
}

