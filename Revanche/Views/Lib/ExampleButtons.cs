﻿using System;
using Revanche.Lib;

namespace Revanche.Views.Lib
{
	public partial class ExampleButtons : Gtk.Bin
	{
		public Gtk.Orientation Orientation{ get; private set;}

		public event EventHandler ExampleCreated;

		public ExampleButtons(Gtk.Orientation orientation)
		{
			this.Orientation=orientation;

			this.Build();

			if(Example.HasProject(ExampleProjects.PRODUCT_CATALOG)) {
				this.button1.Sensitive=false;
			}

			if(Example.HasProject(ExampleProjects.TASK_TRACKER)) {
				this.button2.Sensitive=false;
			}

			if(Example.HasProject(ExampleProjects.CHARACTER_MANAGER)) {
				this.button3.Sensitive=false;
			}
		}

		protected void handleCreateProduct(object sender,EventArgs e)
		{
			Example.CreateProject(ExampleProjects.PRODUCT_CATALOG);
			this.button1.Sensitive=false;
			if(this.ExampleCreated!=null) {
				this.ExampleCreated(this,EventArgs.Empty);
			}
		}

		protected void handleCreateTask(object sender,EventArgs e)
		{
			Example.CreateProject(ExampleProjects.TASK_TRACKER);
			this.button2.Sensitive=false;
			if(this.ExampleCreated!=null) {
				this.ExampleCreated(this,EventArgs.Empty);
			}
		}

		protected void handleCreateCharacter(object sender,EventArgs e)
		{
			Example.CreateProject(ExampleProjects.CHARACTER_MANAGER);
			this.button3.Sensitive=false;
			if(this.ExampleCreated!=null) {
				this.ExampleCreated(this,EventArgs.Empty);
			}
		}
	}
}

