﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Revanche
{
	/// <summary>
	/// Supplies a set of identifiables to the ModelTreeView
	/// </summary>
	public class IdentifiableSet
	{
		public Models.RevModel Model{ get; private set; }
		public Models.Identifiable Parent{ get; private set; }
		public bool Extendable
		{
			get {
				return this.Parent!=null&&this.SameParent&&this.Model!=null&&(this.Parent.GetModelInfo().Collection as IEnumerable<string>).Contains(this.Model.Id)
					|| this.Parent!=null&&this.SameParent&&this.Model==null&&this.Parent.GetModelInfo().Collection.Length>0
					|| this.Parent==null&&this.Model!=null&&Models.RevModel.Collectables.Union(Models.RevModel.NoParents).Contains(this.Model)
					|| this.Parent==null&&this.Model==null&&Models.RevModel.Collectables.Union(Models.RevModel.NoParents).Count()>0;
			}
		}
		public IdentifiableSet(Models.RevModel model,Models.Identifiable parentObj=null)
		{
			if(parentObj!=null) {
				this.Objects=model.GetImplementations().Where(i => i.DescendsFrom(parentObj));
			} else {
				this.Objects=model.GetImplementations();
			}
			this.Columns=model.GetPropertyNames().Where(property=>model.TitleProp!=property&&!Types.RevType.MetaTypes.Contains(model.GetPropertyType(property).Id));
			this.Model=model;
			this.Parent=parentObj;
			this.SameParent=parentObj!=null;
		}
		public IdentifiableSet(Models.Identifiable parentObj, string collection)
		{
			Models.RevModel model=parentObj.GetModelInfo().GetCollection(collection)[0];
			this.Objects=parentObj.GetCollection(collection);
			this.Columns=model.GetPropertyNames().Where(property=>model.TitleProp!=property&&!Types.RevType.MetaTypes.Contains(model.GetPropertyType(property).Id));
			this.Model=model;
			this.Parent=parentObj;
			this.SameParent=true;
		}
		public IdentifiableSet(Models.Identifiable[] objects,Models.Identifiable parentObj=null)
		{
			this.Objects=objects;
			this.Model=null;
			if(objects.Length>0) {
				Models.RevModel firstModel=objects.First().GetModelInfo();
				this.SameParent=true;
				IEnumerable<string> properties=firstModel.GetPropertyNames().Where(property => firstModel.TitleProp!=property&&!Types.RevType.MetaTypes.Contains(firstModel.GetPropertyType(property).Id));
				foreach(Models.Identifiable obj in objects) {
					Models.RevModel model=obj.GetModelInfo();
					IEnumerable<string> localProperties=model.GetPropertyNames().Where(property => model.TitleProp!=property&&!Types.RevType.MetaTypes.Contains(model.GetPropertyType(property).Id));
					properties=properties.ToArray().Where(x => localProperties.Contains(x));
					if(obj.Parent!=parentObj) {
						this.SameParent=false;
					}
				}
				this.Columns=properties;
			} else {
				this.Columns=Enumerable.Empty<string>();
				this.SameParent=true;
			}
			this.Parent=parentObj;
		}

		public IEnumerable<Models.Identifiable> Objects{ get; private set; }
		public IEnumerable<string> Columns{get;private set;}
		public bool SameParent{ get; private set; }
	}
}

