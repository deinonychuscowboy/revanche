Revanche External XDG Integration
=================================

These templates help integrate Revanche with your linux system, providing protocol
handlers for the rvt:// and rvi:// URI protocols. This allows you to launch
Revanche from anywhere and navigate to a particular object associated with some
context.

Revanche does not yet have a real installer, so you must manually move these files
to your ~/.local/share/applications directory (integrating mimeapps.list with the
existing version if there is one). This will add Revanche to your system menu and
add the URI protocols to your system handler.

From the command line, you will now be able to use xdg-open rvt://... and similar.
From within applications and desktop environments that support freedesktop.org,
you should be able to just use rvt://... and similar directly.
