REVANCHE STRUCTURES FOLDER
==========================

Creating a structure is the first step to storing your data in Revanche.

Revanche uses [YAML](http://yaml.org/) to store data. YAML is a very
simple 'markup language', a kind of programming language that provides
both data and additional information about that data. 

Writing a YAML document should be easy, even if you've never programmed
before. It's meant to be very similar to a bulleted list you might jot
down on a notepad.

Brief YAML Tutorial
-------------------

There is no definitive YAML tutorial, although searching for
'YAML Tutorial' in your favorite search engine is pretty helpful. A
quick-and-dirty guide follows.

Everything in YAML is a list. There are two kinds of lists. The first is
a sequence.

```
- Tacos
- Cheese Enchiladas
- Burritos
```

This is a sequence. Sequences are indicated by a hyphen followed by a
space, and then the item, much like a bulleted list on a piece of paper.
Sequences can contain multiple words per item.

The other kind of list is a mapping. In a mapping, each item has both a
name and a value.

```
Guacamole: Delicious
Salsa: Overrated but pleasant
SourCream: Mandatory
```

A mapping is defined without hyphens, and has a colon and a space
separating the name from the value. As you can see, names must be one
word (no spaces), while values can be multiple words.

Sequences and mappings can be nested inside each other without limit. To
nest a sequence or mapping, simply indent it. Make sure to be consistent
with your indents - use either space characters or tab characters, not
both.

Here are some nested mappings and sequences, indented with spaces:

```
- Bananas: Potassium
  Oranges: Vitamin C
  Spinach:
    - Vitamin A
    - Vitamin C
    - Vitamin K
    - Magnesium
    - Manganese
    - Folate
    - Iron
- Burrito:
    - Sour Cream
    - Beef
    - Hot Sauce
    - Cheese: Queso
    - Rice
    - Guac
    - Black Beans
  Taco:
    - Chicken
    - Lettuce
    - Sour Cream
    - Cheese: Queso
    - Hot Sauce
    - Salsa
```

This is essentially what Revanche's structures look like. For your own
purposes, you can also make notes using the # character. Anything on a
line beginning with a # will be ignored; this is called a comment.

```
- Burrito:
    # I prefer burritos to tacos
    - Sour Cream
```

All Revanche YAML documents contain a sequence at the base level, with
additional sequences or mappings nested inside.

Introduction to Revanche Structures
-----------------------------------

Revanche has two structure components: Types and Models. Both are
required to set up data in Revanche. 

Revanche works by showing you screens that correspond to 'objects'.
Each one of these objects can have many 'properties'. In your
structure definition, Models serve as a template for objects,
and Types serve as a template for properties.

You are free to define Models and Types in as many files as you like, with
these expectations:

1. Files containing types begin with the prefix 'types_' and end with
'.yml' or '.yaml'
2. Files containing models begin with the prefix 'models_' and end with
'.yml' or '.yaml'

(The need for index_types and index_models files was removed in v1.4.)

Example Files:
```
types_fruit.yml
types_vegetables.yml
types_meat.yml
models_mexican.yml
models_german.yml
models_chinese.yml
```

Types
-----

Some types are included by default in Revanche. These do not need to be
specified in the structures folder, and are as follows:
```
BOOLEAN   - true or false
DECIMAL   - decimal number
INTEGER   - number with no decimal
TEXT      - raw text data with no fancy handling
TIMESTAMP - date and time information
TRIGGER   - kicks off a trigger
LINK      - link to an object in Revanche or an external URL
RELATION  - link to one of a set of objects
```

Each of these built-in types has special UI rendering that make editing
it easier and more comfortable. These types will probably not be
sufficient for your purposes, but if you do not need any other types in
your models, you can run Revanche with just the built-in types, and skip
this section.

Types are a specification of what values should be options in the UI.
User-defined types are always rendered as a dropdown - if you want a
different UI element, such as a text box, use one of the
specially-handled built-in types above. Types require only three
attributes, in a mapping. Types correspond to individual editable fields
on a Revanche screen.

- Id - This defines the name of the type. The convention for types is to
make this name all caps; please follow it, or Revanche may get confused.
- Values - This defines what values are available to be selected. If you
want to be able to set a type to nothing, you may explicitly list "null"
or an empty value here. You may also set DefaultValue to "null" if you
want empty to be the default.

In addition, the following properties are required in certain situations:

- DefaultValue - This matches one of the items in the Values attribute,
and is required unless you have "null" or an empty value in your Values
list. You may also set DefaultValue to "null" or an empty value, and
leave that value out of your Values list, if you want empty to be the
default.
- NamedValues - For the special COLOR type only, You may also define types
with both a 'name' displayed on the screen, and a 'value' used internally.

Here is an example type list:

```
- Id: VOICE
  Values:
    - Tenor
    - High Baritone
    - Low Baritone
    - High Bass
    - Low Bass
    - Raspy
    - Husky
  DefaultValue: High Baritone
```

Types also have a few optional attributes:

- ShowDefault - This defaults to true. If you set it to false, when anything
using this type is set to its default value, it will not appear in most
Revanche screens. It will still show up in the edit window.
- HelpNotes - Provides helpful information about this type.
- ValueHelpNotes - Provides helpful information about each value this type
specifies.

The two help notes are intended to aid the user in understanding how to
enter data in an edit screen. These values will be displayed in the edit
window, generally in a tooltip.

```
- Id: STATE
HelpNotes: Only states visited during the first book are represented.
Values:
  - null
  - Alaska
  - California
  - Iowa
  - NewJersey
  - NewYork
  - Washington
ShowDefault: true
ValueHelpNotes:
  Alaska: Cold.
  California: Warm.
  Iowa: Lots of corn and wind turbines.
  NewJersey: Jug handles.
  NewYork: It's bigger than you think.
  Washington: Few cities between Spokane and the Cascades.
```



Special handling is included for a type named COLOR. This type uses
NamedValues to provide both color names and hexadecimal color values to
display on the screen, and also uses Values to break this color list
into rows in a UI dialog. DefaultValue is not required for the COLOR
type. The COLOR type is built in - if you do not provide one, you will
receive a default with these colors:
```
Black
Gray
White
Red
Orange
Yellow
Green
Blue
```

This default set of colors is sparse for a reason - these colors have
non-controversial names. So, if you start setting up data using these
default colors, and later decide you need more colors, you can easily
create your own COLOR definition and not worry about data that already
uses one of the default colors, so long as your definition includes the
above colors.

Put another way, 'Purple' is not a default color, because perhaps you
prefer the word 'Violet' for that color.

Here is the COLOR definition I use, as an example of a custom definition:

```
- Id: COLOR
Values:
  - 10
  - 7
  - 8
  - 10
  - 10
NamedValues:
  Black: 000000
  Smoke: 333333
  Gray: 777777
  Silver: bbbbbb
  White: ffffff
  Russet: 771100
  Vermillion: ff3300
  Dusky: bbaa88
  Cream: ffeeaa
  Tawny: cccc55
  Copper: cc6600
  Brown: 773300
  Umber: 331100
  Hazel: 667700
  Green: 007700
  Cadet: 003377
  Steel: 7799bb
  Orchid: dd77ff
  Pink: ff99cc
  Rose: ff8888
  Salmon: ff9977
  Citrine: ddff77
  Spring: bbff77
  Sky: 77ffff
  Periwinkle: bb99ff
  Red: ff0000
  ConstructionYellow: ffcc00
  ElectricYellow: ddff00
  Chartreuse: 77ff00
  NeonGreen: 00ff00
  Somtaaw: 426CCA
  Cobalt: 0000ff
  Indigo: aa00ff
  Fuchsia: ff00ff
  Claret: cc0066
  Gold: 777700
  Viridian: 007733
  Teal: 007777
  Navy: 000077
  Ultramarine: 330077
  Plum: 550077
  Purple: 770077
  Bordeaux: 770055
  Burgundy: 770033
  Maroon: 770011
```

Models
------

Models specify the structure of your objects, and correspond to an
entire screen in Revanche's UI. Models are a template for objects.

Revanche models have several attributes, all but one of which are
optional. The single mandatory attribute is:

- Id - as with types, this defines the name of the model. The convention
for models is to make this CamelCase; please follow it, or Revanche
may get confused.

The optional attributes are as follows:

- Children - This is a sequence of models whose parent is this one. An
object that is based on this Model template will have exactly one object
following each Model in its Children list. The Car model should have one
SteeringWheel model in its Children.
- Collection - This is also a sequence of models whose parent is this one.
Unlike Children, an object may have many copies of each Model listed here.
The Car model should have the Bolt model in its Collections, as a Car has
many Bolts.
- Properties - This is the meat and potatoes of a model definition. The
properties list is a mapping, with the name being shown in the UI and
the value being a type (see the discussion of types above). The
properties list defines the editable fields you will see in the UI, and
refers to the defined types to provide information about those fields.
- Defaults - These provide default values for the properties of this model.
These defaults override any DefaultValue defined in the property's type.
- ShowDefaults - These work similarly to the ShowDefault flag for types,
but on a per-property basis within this model. If you specify a property
as false, it will only show up in the edit window when its value is
the same as its default.
- TitleProp - Revanche needs to be able to display a 'Title' for objects
in the UI, and also look objects up by title in specifiers. By default,
Revanche will use a property named 'Title', or fall back to one named
'Name'. If neither exists, Revanche will use the Id attribute of the
object, which is not human-readable. You can specify a property to use
as the title by adding this attribute.
- Triggers - These represent a definition of actions to be taken when a
field's value changes. See the Triggers section for more on these.
- HelpNotes - Like Types, you can help the user by providing a description
or notes about this model here.
- PropertyHelpNotes - Again, like Types, you can also provide per-property
help.

Here is an example definition of three basic models for the
fiction-writing use case:

```
- Id: World
HelpNotes: May also be called a Universe.
Collection:
  - Project
Properties:
  Name: TEXT
- Id: Project
HelpNotes: A single writing project.
Parent: World
Collection:
  - Character
Properties:
  Summary: TEXT
  Location: LOCATION
  DateTime: TIMESTAMP
Defaults:
  Summary: New Project
ShowDefaults:
  Summary: false
TitleProp: Summary
- Id: Character
Parent: Project
Collection:
  - TropeReference
Children:
  - Coloration
  - Attire
  - Body
  - Behavior
Properties:
  Name: TEXT
  Gender: GENDER
  Age: DECIMAL
  Occupation: TEXT
  Voice: VOICE
PropertyHelpNotes:
  Name: What people generally call this character, may not be their real name.
  Age: In years.
```


In this example, you can see three models: World, Project, and
Character. An instance of World can contain many Projects, an instance
of Project can contain many Characters. The Character model also refers
to a TropeReference model, which it can contain many of, and four other
models, which it contains exactly one of. Child models like these are a good
way to break up properties so you don't get a huge list of things that's
difficult to read through.

Each model also contains a list of properties. If you use CamelCase in
defining your property names, Revanche will automatically split the names
and insert a space before each capital letter in the UI, e.g. "APropertyName"
will be converted to "A Property Name". The values of each property refer to
types. As mentioned above, the TEXT, TIMESTAMP, and DECIMAL types are built-in;
the others would need to be specified in this directory.

Triggers
--------

Triggers are a way to specify that, when a certain property
changes to a certain value, other properties should also be changed to other values.

Triggers have several limitations:
1. They only work on properties. You cannot change any other attributes with them.
2. They are local to an object. That is, a trigger cannot change the property of
a child or parent object - the property that kicks off a trigger and the property it
modifies must be part of the same model.
3. They cannot be chained. If a trigger changes the property FavoriteFood to the value
Burrito, and another trigger exists that is kicked off whenever FavoriteFood changes to
Burrito, the second trigger will not be kicked off by the first.

All three of these limitations are intentional, and reduce the complexity both of 
implementing a trigger system, and avoiding problems like endless loops of triggers
kicking each other off.

Triggers are defined on the Model object, and are a list of mappings. Each mapping has
two attributes:

- Conditions - This is a mapping of Property names to values. Multiple properties may be
specified here - if multiple conditions exist, ANY of the properties changing will
kick off the trigger (boolean or). There is currently no way to specify that a trigger
be kicked off only if two properties are both set to certain values (boolean and), or
if a property is not set to a certain value (boolean not). This ability may be added if
needed.
- Effects - This is a mapping of Property names to values. When any condition matches, ALL
the listed properties will be changed to the listed values.

Triggers also have an affiliated Type, called TRIGGER. This is a built-in type which
you can use in a property to provide a 'go button' to run any triggers defined for
that property. This is useful if you have a trigger, but don't really have a
property that makes sense for its Conditions section, and can be used to implement
mini-templates that set multiple properties to values that are somehow related to
each other.

An example model with both normal triggers and triggers using the TRIGGER type:

```
- Id: Attire
Parent: Character
Properties:
  Shirt: SHIRT
  Pants: PANTS
  MedievalClothes: TRIGGER
  ModernClothes: TRIGGER
Triggers:
  - Conditions:
      Shirt: Robe
    Effects:
      Pants: Skirt
  - Conditions:
      MedievalClothes: TRIGGER
    Effects:
      Shirt: Tunic
      Pants: Hose
  - Conditions:
      ModernClothes: TRIGGER
    Effects:
      Shirt: TShirt
      Pants: Jeans
```

Specifiers, LINK, and RELATION
------------------------------

New in version 1.4, specifiers give you a way to locate specific objects/screens in
Revanche. This goes hand-in-hand with the new LINK type, which displays a button
you can click to travel to a location, and the RELATION type, which allows you to
connect to one object of a given model.

Specifiers take the form:

```
PREFIX:PATH
```

where PREFIX is one of
```
ID
TITLE
```

and PATH is a local or rooted slash-separated path to a buried object. For example,
a local path might look like 'TITLE:child/subchild', while a rooted one would look
like 'TITLE:/noparent/child'.

A specifier, when dereferenced, will look for an object that has the given title or id.
For specifiers using TITLE, the title property is used, meaning a property with the
name 'Name' or 'Title', as described above.

The first example above will look within the current object's children and collections
for an object titled 'child', and then within that object's children for an object
titled 'subchild'. If multiple objects with this title exist, only one is returned.
The second example above will instead look within the list of all objects that have no
parent, displayed on the home screen, for an object titled 'noparent', and then within
that object's children and collections for an object titled 'child'.

The 'TITLE' prefix is optional, and may be omitted. ':/noparent/child' is a valid
specifier, and will do the same thing as if TITLE was specified.

If you have objects with the same title that could be confused, or if the title of an
object might change over time, you can use ID to specify the real ID of the object.
An object's ID is visible on its Edit dialog. You must use either all IDs or all
titles within one specifier, the two cannot be mixed and matched.

ID specifiers look like 'ID:/12345678-90ab-cdef-1234-567890abcdef'. They do need an
initial slash in most cases (no initial slash means local, as with title), but should
not have more than the one slash; there is no need to construct a full path of objects.
For this reason, ID specifiers may also be useful to access very deeply nested
objects that would have long and confusing TITLE specifiers.

Outside of Revanche, if you have the necessary mimetypes and handlers configured,
you may wish to use the rvt:// and rvi:// form of specifiers, which look and act
more like traditional URIs so that other programs can understand them. rvt://
corresponds to TITLE: and rvi:// corresponds to ID:. You may follow these protocols
with an optional profile and path like normal.

```
rvt:///noparent/child
rvi:///12345678-90ab-cdef-1234-567890abcdef
rvt://profile:/noparent/child
rvi://profile:/12345678-90ab-cdef-1234-567890abcdef
```

Specifiers may be passed on the command line when Revanche starts, or they may be
used via the LINK type, going in the edit form field for the LINK. LINK will render a
button which can be clicked to take you to the object specified. LINK also supports
URLs, and will open these in the user's preferred browser.

Similarly, you may also use the RELATION type to connect to other objects. LINK takes
a free-form text value, and you are responsible for creating a valid specifier for it.
If you know (for instance) that your connected object is of a particular model, you
can instead use RELATION to display a dropdown with all objects implementing that
model (or models), so that you do not need to construct the specifier yourself. You
can also use an 'untyped' relation if you want to display a list of all objects in
Revanche.

```
- Id: Character
Properties:
  FavoriteTeacher: RELATION Character
  BestFriend: RELATION Character Pet
  RandomObject: RELATION
```

Closing
-------

For more examples of types and models, please refer to the example structures, which
may be created from the profile window.
